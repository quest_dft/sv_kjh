import function_list as cf
import requests

import argparse
import sys
import re
from dateutil.parser import parse
import datetime

parser = argparse.ArgumentParser()


parser.add_argument('-u', type=str)
parser.add_argument('-ss', type=str)

group = parser.add_mutually_exclusive_group()

group.add_argument("-i", "--item", action="store_true")
group.add_argument("-p", "--paid", action="store_true")
group.add_argument("-c", "--cancel", action="store_true")
group.add_argument("-r", "--orderreturn", action="store_true")
group.add_argument("-csc", "--cscollect", action="store_true")
group.add_argument("-s", "--settlement", action="store_true")
group.add_argument("-css", "--cssend", action="store_true")
group.add_argument("-sv", "--sendinvoice", action="store_true")

parser.add_argument('args', nargs='*')
args = parser.parse_args()

# try:
#     cf.Auction.set_private_key(getattr(cf, 'PRIVATE_KEY_'+args.name))
# except AttributeError:
#     sys.exit('INVALID NAME ERROR!!')

url = f"http://goup.finejin.com/ADMIN/api/user_conf.php?UserSeq={args.u}&ServiceShopSeq={args.ss}"

# Here
js_on = requests.get(url).json()
NAME = js_on['UserName']

cf.Auction.set_private_key(js_on['Key']['secret_key'])
cf.Auction.set_u_and_ss(args.u, args.ss)
cf.Auction.change_save_dir(args.u, args.ss)

if NAME not in cf.Auction.check_valid():
    raise Exception('FATAL ERROR!!! WRONG USER')


def compile_to_date(args):
    pattern = re.compile('^\d{4}-\d{2}-\d{2}')

    match_num = 0
    for idx in range(len(args)):
        if bool(pattern.match(args[idx])):
            match_num += 1
            try:
                args[idx] = parse(args[idx])
            except ValueError:
                sys.exit('INVALID DATE')

    if match_num == 0:
        now = datetime.datetime.utcnow()+datetime.timedelta(hours=9)
        return [now - datetime.timedelta(days=7), now] + args
    elif match_num == 2:
        return args
    else:
        raise KeyError('Wrong args')


compiled_args = compile_to_date(args.args)

if args.item:
    cf.Auction.get_selling_item_list()
elif args.paid:
    cf.Auction.get_paid_order_list(*compiled_args)
elif args.cancel:
    cf.Auction.get_order_canceled_list(*compiled_args)
elif args.orderreturn:
    cf.Auction.get_return_list(*compiled_args)
elif args.cscollect:
    cf.Auction.get_seller_qna_list(*compiled_args)
elif args.settlement:
    cf.Auction.get_settlement_list(*compiled_args)
elif args.cssend:
    cf.Auction.add_qna_reply(*compiled_args)
elif args.sendinvoice:
    cf.Auction.do_shpping_general(*compiled_args)