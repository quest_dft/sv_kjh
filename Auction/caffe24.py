class Caffe24JE:
    @staticmethod
    def selling_item(json, total_tuple, xlsx_stream, start_row_num):
        row_num = start_row_num[0]
        idx_ = ('Index',)

        # Total_tuple must be input
        total_tuple = ('shop_no', 'product_no', 'product_code', 'custom_product_code', 'product_name',
                       'eng_product_name', 'model_name', 'price', 'retail_price', 'display', 'selling',
                       'product_used_month', 'summary_description', 'price_content', 'buy_unit_type', 'buy_unit',
                       'order_quantity_limit_type', 'minimum_quantity', 'maximum_quantity', 'mileage_amount',
                       'adult_certification', 'detail_image', 'list_image', 'tiny_image', 'small_image', 'has_option',
                       'option_type', 'manufacturer_code', 'trend_code', 'brand_code', 'made_date',
                       'expiration_date_start_date', 'expiration_date_end_date', 'origin_classification',
                       'origin_place_no', 'origin_place_value', 'icon_show_period_start_date',
                       'icon_show_period_end_date', 'icon', 'product_material', 'list_icon_soldout_icon',
                       'list_icon_recommend_icon', 'list_icon_new_icon', 'approve_status', 'sold_out')

        # Must erase this line after request method implementation
        total_tuple = idx_ + total_tuple
        tmp_mapper = {v: k for k,v in enumerate(total_tuple)}
        length = len(total_tuple)

        for prod in json['products']:
            write_list = ['' for _ in range(length)]
            for k, v in prod.items():
                if type(v) is list:
                    write_list[tmp_mapper[k]] = '$'.join(str(v).strip())
                elif type(v) is dict:
                    for k_, v_ in v.items():
                        write_list[tmp_mapper[k_]] = str(v_).strip()
                else:
                    write_list[tmp_mapper[k]] = str(v).strip()
            write_list[0] = str(row_num)
            Caffe24JE._excel_write_row(xlsx_stream, row_num, write_list)
            row_num += 1

        start_row_num[0] = row_num

        # Must close worksheet

    @staticmethod
    def paid_returned_cancelled(json, total_tuple, xlsx_stream, start_row_num):
        pass

    @staticmethod
    def _excel_write_row(work_sheet, row_num, row, cell_format=None):
        for col in range(len(row)):
            work_sheet.write(row_num, col, row[col], cell_format)

