### Auction
> Dependency

1. xlsxwriter
2. dateutil (pip3 install python-dateutil)
> Run code
<pre><code>
python3 mapper.y -u "userseq" -ss "serviceshopseq" {func} {additional args}
</code></pre>
> Example1
<pre><code>
python3 mapper.y -u 1008 -ss 1001 -i
</code></pre>
> Example2
<pre><code>
python3 mapper.y -u 1008 -ss 1001 -p 2018-10-01 2018-11-08
</code></pre>

> u and ss

-u : userseq, 이지플레이사용자 고유번호

1008: 더싸다구

-ss : serviceshopseq, 사용자 등록 쇼핑몰 고유번호

No | 1000 | 1001 | 1002 | 1003 | 1004 | 1005 | 1006 | 1007 | 1008 | 1009 | 
--- | --- | --- | --- |--- |--- |--- |--- |--- |--- |--- |
이름 |  G마켓 | 옥션 | 11번가 | 메이크샵 | 티몬 | 쿠팡 | 카카오쇼핑 | 네이버페이 | 네이버스토어 | 위메프 | 




>func

startdate, enddate 입력이 없을경우 startdate: 현재시각 - 7일 , enddate: 현재시각 (한국시간)

date 입력 양식 : 2018-01-02

-i : item 상품수집

-p : paid 주문수집

-c : cancel 취소수집

-r : orderreturn 반품수집

-csc : cscollect  cs 수집

-css :  cssend : cs
