#!/usr/bin/env bash

declare -A args=( ["-i"]="" ["-p"]="" ["-c"]="" ["-r"]="" ["-csc"]="" ["-s"]="")
filename=`basename $0`
new_file=${filename%.*}
for args_k in ${!args[@]}
do
    echo ${args_k}
    python3 ${new_file}_args.py -u 1008 -ss 1001 ${args_k} ${args[$args_k]}
    echo -e 'Done\n'
done
