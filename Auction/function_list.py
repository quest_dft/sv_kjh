import requests
import xml.etree.ElementTree as ET
import xlsxwriter
import datetime
import sys
from itertools import chain
from time import sleep
from collections import defaultdict
import os


# TODO: text xml mapping -> 내일

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))
from utilty import Util
from seleniumcrawler import SeleniumCrawler

HEADERS = {'Content-Type': 'text/xml'}
AUCTION_URL = 'http://api.auction.co.kr/APIv1/AuctionService.asmx'
AUCTION_URL_S = 'http://api.auction.co.kr/APIv1/ShoppingService.asmx'


def get_soap(url, header, body):
    return requests.post(url, data=body, headers=header, stream=True)


class Auction(SeleniumCrawler):
    PRIVATE_KEY = None
    HEADERS = {'Content-Type': 'text/xml'}
    URL = AUCTION_URL
    URL_S = AUCTION_URL_S
    SLEEP_TIME = 0.1 # Sec
    DEFAULT_SAVE_DIR = ''

    USERSEQ = ""
    SERVICESHOPSEQ = ""

    @staticmethod
    def set_u_and_ss(u, ss):
        Auction.USERSEQ = u
        Auction.SERVICESHOPSEQ = ss

    @staticmethod
    def get_excel_path(excel_name):
        return os.path.join(Auction.DEFAULT_SAVE_DIR, '{}_{}_{}_{}.xlsx'.format(Auction.USERSEQ, Auction.SERVICESHOPSEQ,
                            excel_name, (datetime.datetime.utcnow()
                                            + datetime.timedelta(hours=9)).strftime('%Y%m%d%H%M%S')))

    @staticmethod
    def set_private_key(key):
        Auction.PRIVATE_KEY = key

    @staticmethod
    def set_url(url):
        Auction.URL = url

    @staticmethod
    @Util.Report
    def get_return_list(date1, date2):
        body = """<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
      <Value>{keys}</Value>
    </EncryptedTicket>
  </soap:Header>
  <soap:Body>
    <GetReturnList xmlns="http://www.auction.co.kr/APIv1/AuctionService">
      <req SearchType="None" SearchDateType="Request" >
<SearchDuration StartDate="{date1}" EndDate="{date2}" xmlns="http://schema.auction.co.kr/Arche.APISvc.xsd" />
      </req>
    </GetReturnList>
  </soap:Body>
</soap:Envelope>
        """.format(**{"keys": Auction.PRIVATE_KEY, "date1": date1.strftime('%Y-%m-%d'),
                      "date2": date2.strftime('%Y-%m-%d')})
        result = get_soap(Auction.URL, Auction.HEADERS, body)
        result.raw.decode_content = True
        note = ET.iterparse(result.raw)

        index_tuple_ = ('Index', )
        buyer_key_list = ('Name', 'Tel', 'MobileTel', 'Email', 'PostNo', 'AddressPost',
                          'AddressDetail', 'AddressRoadName')
        order_key_list = ('ItemID', 'OrderNo', 'ItemName', 'AwardQty', 'AwardAmount', 'DeliveryFeeAmount', 'BuyerName',
                          'BuyerID', 'GroupOrderSeqno', 'RequestOption', 'RequestOptionPrice', 'SellerStockCode',
                          'OrderDate')
        return_key_list = ('FastRefundyn', 'ReturnStatus', 'RequestedDate', 'WithdrawDate', 'ReturnDate',
                           'ReturnArrivalDate', 'PayNo', 'RecieverName', 'FirstDeliveryFeeCharge', 'ReturnFeeCharge',
                           'ReturnRequester', 'ReturnReasonCode', 'ReturnDetailReason', 'ReturnDeliveryMethod',
                           'BuyerRequestMemo', 'DeliveryCompanyName', 'DeliveryNo', 'TrustSellerType', 'PaymentChoice',
                           'ReturnFeeAmount', 'RetPickupType')

        sum_tuple = index_tuple_ + buyer_key_list + order_key_list + return_key_list

        xlsx_name = Auction.get_excel_path('orderreturn')
        write_crit = ('Buyer', 'Order')
        end_crit = ('ReturnList',)

        Auction._make_into_excel(note, sum_tuple, xlsx_name, write_crit, end_crit)

    @staticmethod
    @Util.Report
    def get_order_canceled_list(date1, date2):
        body = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
      <Value>{keys}</Value>
    </EncryptedTicket>
  </soap:Header>
  <soap:Body>
    <GetOrderCanceledList xmlns="http://www.auction.co.kr/APIv1/AuctionService">
      <req SearchType="Nothing">
        <SearchDuration StartDate="{date1}" EndDate="{date2}" xmlns="http://schema.auction.co.kr/Arche.APISvc.xsd" />
      </req>
    </GetOrderCanceledList>
  </soap:Body>
</soap:Envelope>"""
        body_format_dict = {'keys': Auction.PRIVATE_KEY, 'date1':'', 'date2':''}

        index_tuple = ('Index',)
        orderbase_tuple = ('ItemID', 'OrderNo', 'ItemName', 'AwardQty', 'AwardAmount', 'DeliveryFeeAmount', 'BuyerName',
                           'BuyerID', 'GroupOrderSeqno', 'RequestOption', 'RequestOptionPrice', 'SellerStockCode',
                           'OrderDate')
        ordercanceled_tuple = ('CategoryName', 'CancelType', 'CancelDate')

        sum_tuple = index_tuple + orderbase_tuple + ordercanceled_tuple

        xlsx_name = Auction.get_excel_path('cancel')
        write_crit = ('OrderBase',)
        end_crit = ('OrderCanceled',)
        time_seq = 3

        Auction._make_into_excel_time_limit_case(body, body_format_dict, date1, date2, time_seq, sum_tuple,
                                                 xlsx_name, write_crit, end_crit)

    @staticmethod
    @Util.Report
    def get_paid_order_list(start_time, end_time):
        body="""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
      <Value>{keys}</Value>
    </EncryptedTicket>
  </soap:Header>
  <soap:Body>
    <GetPaidOrderList xmlns="http://www.auction.co.kr/APIv1/AuctionService">
      <req DurationType="ReceiptDate" SearchType="Nothing">
        <SearchDuration StartDate="{date1}" EndDate="{date2}" xmlns="http://schema.auction.co.kr/Arche.APISvc.xsd" />
      </req>
    </GetPaidOrderList>
  </soap:Body>
</soap:Envelope>"""
        body_format_dict = {'keys': Auction.PRIVATE_KEY, 'date1': "", 'date2': ""}

        index_tuple = ('Index',)
        orderbase_tuple = ('ItemID', 'OrderNo', 'ItemName', 'AwardQty', 'AwardAmount', 'DeliveryFeeAmount', 'BuyerName',
                           'BuyerID', 'GroupOrderSeqno', 'RequestOption',' RequestOptionPrice', 'SellerStockCode',
                           'OrderDate')
        addressbase_tuple = ('Name', 'Tel', 'MobileTel', 'Email', 'PostNo', 'AddressPost', 'AddressDetail',
                             'AddressRoadName')
        paidorder_tuple = ('DeliveryFee', 'DeliveryFeeAmount', 'ReceiptDate', 'BundleDelvieryRequestNo',
                           'RequestOption', 'SellerStockCode', 'DeliveryRemark', 'DeliveryRemark', 'DeliveryDelayDay',
                           'DeliveryDelayNotifyDate', 'SellerId', 'ItemCode', 'OrderDate', 'PayNo', 'GroupOrderSeqno',
                           'TransDueDate', 'TransPolicyType')

        sum_tuple = index_tuple + orderbase_tuple + addressbase_tuple + paidorder_tuple

        xlsx_name = Auction.get_excel_path('paid')
        write_crit = ('OrderBase', 'AddressBase')
        end_crit = ('PaidOrder',)
        time_seq = 27
        col_map = {}
        _tu1 = [col_map.get(i, i) for i in sum_tuple]

        date_difference = (end_time - start_time).days
        time_range = [start_time + datetime.timedelta(x) for x in chain(range(0, date_difference, time_seq),
                                                                        [date_difference + 1])]
        def __tmp(x):
            x[1] -= datetime.timedelta(1)
            return x

        time_range = map(__tmp, [time_range[i:i + 2] for i in range(len(time_range) - 1)])

        index_tuple = tuple(enumerate(sum_tuple))
        length = len(index_tuple)

        write_list = ['' for _ in range(length)]
        idx = 0
        write_list[0] = str(idx)

        row_num = 0
        workbook = xlsxwriter.Workbook(xlsx_name, {'constant_memory': True})
        col_format = workbook.add_format({'bold': True})
        worksheet = workbook.add_worksheet()

        # get_shipping_detail
        sum_tuple2 = ('OptionContent', 'BundleDeliveryNo', 'Remark', 'ReturnApproveDate', 'AddressRoadName')
        col_map2 = {}
        _tu2 = [col_map2.get(i, i) for i in sum_tuple2]

        sum_tuple3 = ('Name', 'Tel', 'MobileTel', 'Email', 'PostNo', 'AddressPost', 'AddressDetail', 'AddressRoadName')
        col_map3 = {}
        buyer_ = ['buyer_' + col_map3.get(i, i) for i in sum_tuple3]
        reciver_ = ['reciver_' + col_map3.get(i, i) for i in sum_tuple3]

        transfee_tuple = ('GroupOrderSeqno', 'OrderShippingPaymentType')

        process_tuple = ('EventDate', 'Detail')
        prodoption_tuple = ('OptionName', 'OptionItemName', 'OptionItemPrice', 'OptionQuantity')
        paymentexchange_tuple = ('PaymentDate', 'PaymentFeeType')
        col_map4 ={}
        _tu3 = [col_map4.get(i, i) for i in transfee_tuple + process_tuple + prodoption_tuple + paymentexchange_tuple]

        Auction._excel_write_row_const_memory(row_num, _tu1 + _tu2 + buyer_ + reciver_ + _tu3, worksheet, col_format)

        sum_2_enumerate = tuple(enumerate(sum_tuple2))

        buyer_index = len(_tu2)
        reciver_index = buyer_index + len(buyer_)
        buy_sel_enumerate = tuple(enumerate(sum_tuple3))

        transfee_enumerate = tuple(enumerate(transfee_tuple, reciver_index + len(reciver_)))

        multi_value_col_index = reciver_index + len(reciver_) +len(transfee_tuple)
        multi_value_enumnerate = tuple(enumerate(process_tuple + prodoption_tuple + paymentexchange_tuple,
                                                 multi_value_col_index))

        total_length = len(_tu2) + len(buyer_) + len(reciver_) + len(_tu3)

        def _get_shipping_detail(orderno):
            body = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
      <Value>{keys}</Value>
    </EncryptedTicket>
  </soap:Header>
  <soap:Body>
    <GetShippingDetail xmlns="http://www.auction.co.kr/APIv1/AuctionService">
      <req OrderNo="{orderno}">
      </req>
    </GetShippingDetail>
  </soap:Body>
</soap:Envelope>""".format(**{'keys': Auction.PRIVATE_KEY, 'orderno': orderno})
            result = get_soap(Auction.URL, Auction.HEADERS, body)
            result.raw.decode_content = True
            note = ET.iterparse(result.raw)

            write_list = ['' for i in range(total_length)]

            for event, elem in note:
                if event == 'end':
                    if elem.tag.endswith('Buyer'):
                        for idx, key in buy_sel_enumerate:
                            write_list[idx + buyer_index] = elem.attrib.get(key, '').strip()
                    elif elem.tag.endswith('Reciver'):
                        for idx, key in buy_sel_enumerate:
                            write_list[idx + reciver_index] = elem.attrib.get(key, '').strip()
                    elif elem.tag.endswith(('Processes', 'ProdOption', 'PaymentExchange')):
                        keys_ = elem.attrib.keys()
                        for idx, key in multi_value_enumnerate:
                            if key in keys_:
                                write_list[idx + multi_value_col_index] +='$'+elem.attrib[key].strip()
                    elif elem.tag.endswith('TransFee'):
                        for idx, key in transfee_enumerate:
                            write_list[idx] = elem.attrib.get(key, '').strip()
                    elif elem.tag.endswith('GetShippingDetailResult'):
                        for idx, key in sum_2_enumerate:
                            write_list[idx] = elem.attrib.get(key, '').strip()
                elem.clear()
            return write_list

        dict_ = {}
        for s_time, e_time in time_range:
            try:
                body_format_dict['date1'] = s_time.strftime('%Y-%m-%d')
                body_format_dict['date2'] = e_time.strftime('%Y-%m-%d')
            except KeyError:
                sys.exit('date1 and date2 must be in key')
            body_f = body.format(**body_format_dict)
            result = get_soap(Auction.URL, Auction.HEADERS, body_f)
            result.raw.decode_content = True
            note = ET.iterparse(result.raw)

            # Sleep between query
            sleep(Auction.SLEEP_TIME)

            orderno = ''
            for event, elem in note:
                if event == 'end':
                    if elem.tag.endswith(write_crit):
                        orderno = elem.attrib['OrderNo']
                        dict_ = dict(dict_, **elem.attrib)
                    elif elem.tag.endswith(end_crit):
                        dict_ = dict(dict_, **elem.attrib)
                        for index, key in index_tuple:
                            write_list[index] = dict_.get(key, "").strip()

                        idx += 1
                        write_list[0] = str(idx)

                        row_num += 1
                        Auction._excel_write_row_const_memory(row_num, write_list, worksheet)

                        write_list = ['' for _ in range(length)] + _get_shipping_detail(orderno)
                        dict_ = {}
                elem.clear()
        workbook.close()


    @staticmethod
    def _make_into_excel(note, sum_tuple, xlsx_name, write_crit, end_crit, col_map={}):
        index_tuple = tuple(enumerate(sum_tuple))
        length = len(index_tuple)

        write_list = ['' for _ in range(length)]
        idx = 0
        write_list[0] = str(idx)

        row_num = 0
        workbook = xlsxwriter.Workbook(xlsx_name, {'constant_memory': True})
        col_format = workbook.add_format({'bold': True})
        worksheet = workbook.add_worksheet()

        Auction._excel_write_row_const_memory(row_num, [col_map.get(i, i) for i in sum_tuple], worksheet, col_format)

        dict_ = {}
        for event, elem in note:
            if event == 'end':
                if elem.tag.endswith(write_crit):
                    dict_ = dict(dict_, **elem.attrib)
                elif elem.tag.endswith(end_crit):
                    dict_ = dict(dict_, **elem.attrib)
                    for index, key in index_tuple:
                        write_list[index] = dict_.get(key, "").strip()

                    idx += 1
                    write_list[0] = str(idx)

                    row_num += 1
                    Auction._excel_write_row_const_memory(row_num, write_list, worksheet)

                    write_list = ['' for _ in range(length)]
                    dict_ = {}
            elem.clear()
        workbook.close()

    @staticmethod
    @Util.Report
    def get_selling_item_list():
        body="""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    
    <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
      <Value>{keys}</Value>
    </EncryptedTicket>
  </soap:Header>
  <soap:Body>
    <GetSellingItemList xmlns="http://www.auction.co.kr/APIv1/ShoppingService">
      <req Version="1">
        
        <SearchCondition SearchType="None" xmlns="http://schema.auction.co.kr/Arche.Sell3.Service.xsd" />
      </req>
    </GetSellingItemList>
  </soap:Body>
</soap:Envelope>""".format(**{'keys': Auction.PRIVATE_KEY})

        result = get_soap(Auction.URL_S, Auction.HEADERS, body)
        result.raw.decode_content = True
        note = ET.iterparse(result.raw)

        index_tuple_ = ('Index',)
        itemlist_tuple = ('ItemNo', 'ItemName', 'CategoryCode', 'CategoryName', 'SellingPrice', 'ShippingCost',
                          'ShippingFeeType', 'ShippingFeeChargeType', 'ItemRegistDate', 'SumStockQty', 'OrderQty',
                          'OrderCompleteQty', 'ListingBeginDate', 'ListingEndDate', 'SellingStatusCode',
                          'OrderTypeCode', 'Premium', 'Recommend', 'ManagementCode', 'BrandName', 'ModelName',
                          'IsShippingPrePayable', 'PremiumPlus', 'ListingBeginDateReservation',
                          'ListingEndDateReservation', 'PresaleShippingDate', 'ShppingCondition', 'MinBuyQty',
                          'IsArrival', 'FreeGift', 'OptionTypeCode', 'PenaltyYn', 'TransPolicyNo')

        sum_tuple = index_tuple_ + itemlist_tuple

        xlsx_name = Auction.get_excel_path('item')
        write_crit = ()
        end_crit = ('ItemList', )

        Auction._make_into_excel(note, sum_tuple, xlsx_name, write_crit, end_crit)

    @staticmethod
    def do_shpping_general(req, remittancemethod, shippingmethod):
        # 송장전송
        # http://api.auction.co.kr/APIv1/AuctionService.asmx?op=DoShippingGeneral
        # DeliveryAgency val : nothing or korex or hyundai or epost or dongbu or cjgls or kgb or yellow or hanjin or
        #                      kgbls or sagawa or daesin or ilyang or kyungdong or chonil or gtx or tgs or etc or
        #                      hapdong or cvsnet or samsung or lgelec or slx or dhl or fedex or ems or dwe or usps or
        #                      ltglobal or cjglobal or honam or bhp or gps or hanips or swglobal or kunyoung or
        #                      wizwa or ups or warpex or gsmnton or sebang or sebang10 or aci or gexpress or shiptrack

        body = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
      <Value>{keys}</Value>
    </EncryptedTicket>
  </soap:Header>
  <soap:Body>
    <DoShippingGeneral xmlns="http://www.auction.co.kr/APIv1/AuctionService">
      <req {req_key} >
        <RemittanceMethod {remittancemethod_key} xmlns="http://schema.auction.co.kr/Arche.APISvc.xsd" />
        <ShippingMethod {shippingmethod_key} xmlns="http://schema.auction.co.kr/Arche.APISvc.xsd" />
      </req>
    </DoShippingGeneral>
  </soap:Body>
</soap:Envelope>"""

        req_key = ('OrderNo', 'RetPostNo', 'RetFrontAddr', 'RetBackAddr', 'RetTelNo', 'RetMobileNo', 'RetPickupType')
        req_key = ' '.join(['%s="%s"' % (key, req[key]) for key in req_key if key in req])

        remittancemethod_key = ('RemittanceMethodType', 'RemittanceAccountName', 'RemittanceAccountNumber',
                                'RemittanceBankCode')
        remittancemethod_key = ' '.join(['%s="%s"' % (key, remittancemethod[key]) for key in remittancemethod_key if
                                         key in remittancemethod])

        shippingmethod_key = ('SendDate', 'InvoiceNo', 'MessageForBuyer', 'ShippingMethodClassficationType',
                              'DeliveryAgency', 'DeliveryAgencyName', 'ShippingEtcMethod', 'ShippingEtcAgencyName')
        shippingmethod_key = ' '.join(['%s="%s"' % (key, shippingmethod[key]) for key in shippingmethod_key
                                       if key in shippingmethod])
        body_f = body.format(**{'keys': Auction.PRIVATE_KEY, 'req_key': req_key,
                                'remittancemethod_key': remittancemethod_key, 'shippingmethod_key': shippingmethod_key})
        request = get_soap(Auction.URL, Auction.HEADERS, body_f)

        return ET.fromstring(request.content.decode('utf-8'))[1][0][0].attrib

    @staticmethod
    def add_qna_reply(itemid, questionno, title, content):
        # http://api.auction.co.kr/APIv1/AuctionService.asmx?op=GetSellerQnAList
        body = """
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
      <Value>{keys}</Value>
    </EncryptedTicket>
  </soap:Header>
  <soap:Body>
    <AddQnAReply xmlns="http://www.auction.co.kr/APIv1/AuctionService">
      <req ItemID="{itemid}" QuestionNo="{questionno}" Title="{title}" Content="{content}">
      </req>
    </AddQnAReply>
  </soap:Body>
</soap:Envelope>""".format(**{'keys': Auction.PRIVATE_KEY, 'itemid': itemid, 'questionno': questionno, 'title': title,
                              'content': content})
        request = get_soap(Auction.URL, Auction.HEADERS, body)

        return ET.fromstring(request.content.decode('utf-8'))[1][0][0].attrib


    @staticmethod
    def check_valid():
        body = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Header>
		<EncryptedTicket xmlns="http://www.auction.co.kr/Security">
			<Value>{}</Value>
		</EncryptedTicket>
	</soap:Header>
	<soap:Body>
		<GetMyAccount xmlns="http://www.auction.co.kr/APIv1/AuctionService">
			<req>
			</req>
		</GetMyAccount>
	</soap:Body>
</soap:Envelope>""".format(Auction.PRIVATE_KEY)
        request = get_soap(Auction.URL, Auction.HEADERS, body)
        return ET.fromstring(request.content.decode('utf-8'))[1][0][0][0].attrib['AccountName']




    @staticmethod
    def _get_orderno_paid_list(date1, date2):

        order_no_list = []

        body = """<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
          <soap:Header>
            <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
              <Value>{keys}</Value>
            </EncryptedTicket>
          </soap:Header>
          <soap:Body>
            <GetPaidOrderList xmlns="http://www.auction.co.kr/APIv1/AuctionService">
              <req DurationType="ReceiptDate" SearchType="Nothing">
                <SearchDuration StartDate="{date1}" EndDate="{date2}" xmlns="http://schema.auction.co.kr/Arche.APISvc.xsd" />
              </req>
            </GetPaidOrderList>
          </soap:Body>
        </soap:Envelope>"""
        body_format_dict = {'keys': Auction.PRIVATE_KEY, 'date1': "", 'date2': ""}

        date_difference = (date2-date1).days
        time_range = [date1 + datetime.timedelta(x) for x in chain(range(0, date_difference, 27), [date_difference+1])]

        def __tmp(x):
            x[1] -= datetime.timedelta(1)
            return x

        time_range = map(__tmp, [time_range[i:i + 2] for i in range(len(time_range) - 1)])

        for s_time, e_time in time_range:
            sleep(Auction.SLEEP_TIME)
            body_format_dict['date1'] = s_time.strftime('%Y-%m-%d')
            body_format_dict['date2'] = e_time.strftime('%Y-%m-%d')
            body_f = body.format(**body_format_dict)
            result = get_soap(Auction.URL, Auction.HEADERS, body_f)
            result.raw.decode_content = True
            note = ET.iterparse(result.raw)
            for event, elem in note:
                if event == 'end' and elem.tag.endswith('OrderBase'):
                    if 'OrderNo' in elem.attrib.keys():
                        order_no_list.append(elem.attrib['OrderNo'])

        return order_no_list

    @staticmethod
    @Util.Report
    def get_settlement_list(date1, date2, col_map={}):
        body = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
      <Value>{keys}</Value>
    </EncryptedTicket>
  </soap:Header>
  <soap:Body>
    <GetSettlementDetail xmlns="http://www.auction.co.kr/APIv1/AuctionService">
      <req orderNo="{orderno}">
      </req>
    </GetSettlementDetail>
  </soap:Body>
</soap:Envelope>"""

        index_tuple_ = ('Index', )
        getsettlement_tuple = ('SellFeeAmount', 'DirectDiscountAmount', 'EtcFeeAmount', 'NoInterestFeeAmount',
                               'PayItOnceDiscount', 'SellerPointAmount', 'SellerCouponDiscount',
                               'SellerCouponDiscount1', 'SellerCouponDiscount2', 'BulkDiscount', 'BestMemberDiscount',
                               'MinusEmoney', 'MoneyTransferMethod', 'RemitExpectMoney')
        orderbase_tuple = ('ItemID', 'OrderNo', 'ItemName', 'AwardQty', 'AwardAmount', 'DeliveryFeeAmount', 'BuyerName',
                           'BuyerID', 'GroupOrderSeqno', 'RequestOption', 'RequestOptionPrice', 'SellerStockCode',
                           'OrderDate')
        transfee_tuple = ('GroupOrderSeqno', 'OrderShippingPaymentType')

        option_tuple = ('OptionName', 'OptionQty', 'OptionPrice')
        prodop_tuple = ('OptionName', 'OptionItemName', 'OptionItemPrice')

        sum_tuple = index_tuple_ + getsettlement_tuple + orderbase_tuple + transfee_tuple + option_tuple + prodop_tuple

        index_tuple = tuple(enumerate(index_tuple_ + getsettlement_tuple + orderbase_tuple + transfee_tuple))
        index2_tuple = tuple(enumerate(option_tuple + prodop_tuple, len(index_tuple)))

        length = len(index_tuple) + len(index2_tuple)
        write_list = ['' for _ in range(length)]
        idx = 0
        write_list[0] = str(idx)
        dict_ = {}

        row_num = 0
        xlsx_path = Auction.get_excel_path('settlement')
        workbook = xlsxwriter.Workbook(xlsx_path, {'constant_memory': True})
        col_format = workbook.add_format({'bold': True})
        worksheet = workbook.add_worksheet()

        Auction._excel_write_row_const_memory(row_num, [col_map.get(i, i) for i in sum_tuple], worksheet, col_format)

        body_format_dict = {'keys': Auction.PRIVATE_KEY, 'orderno': ''}
        order_no_list = Auction._get_orderno_paid_list(date1, date2)

        for orderno in order_no_list:
            body_format_dict['orderno'] = orderno
            body_f = body.format(**body_format_dict)

            # Sleep between querying several items
            sleep(Auction.SLEEP_TIME)

            result = get_soap(Auction.URL, Auction.HEADERS, body_f)
            result.raw.decode_content = True
            note = ET.iterparse(result.raw)
            for event, elem in note:
                if event == 'end':
                    if elem.tag.endswith(('OrderBase', 'TransFee')):
                        dict_ = dict(dict_, **elem.attrib)
                    elif elem.tag.endswith(('Options', 'ProdOption')):
                        for index, key in index2_tuple:
                            write_list[index] += "$" + elem.attrib.get(key, '').strip()
                    elif elem.tag.endswith('GetSettlementDetailResult'):
                        dict_ = dict(dict_, **elem.attrib)
                        for index, key in index_tuple:
                            write_list[index] = dict_.get(key, "").strip()

                        idx += 1
                        write_list[0] = str(idx)

                        row_num += 1
                        Auction._excel_write_row_const_memory(row_num, write_list, worksheet)

                        write_list = ['' for _ in range(length)]
                        dict_ = {}
                elem.clear()
        workbook.close()

    @staticmethod
    @Util.Report
    def get_seller_qna_list(date1, date2, col_map={}):
        body = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
      <Value>{keys}</Value>
    </EncryptedTicket>
  </soap:Header>
  <soap:Body>
    <GetSellerQnAList xmlns="http://www.auction.co.kr/APIv1/AuctionService">
      <req  SearchQueryType="All" AnsweredState="All" SortType="None">
        <Duration StartDate="{date1}" EndDate="{date2}" xmlns="http://schema.auction.co.kr/Arche.APISvc.xsd" />
        <ItemIDs xmlns="http://schema.auction.co.kr/Arche.APISvc.xsd">null</ItemIDs>
      </req>
    </GetSellerQnAList>
  </soap:Body>
</soap:Envelope>"""
        body_format_dict = {'keys': Auction.PRIVATE_KEY, 'date1': "", 'date2': ""}

        date_difference = (date2 - date1).days
        time_range = [date1 + datetime.timedelta(x) for x in
                      chain(range(0, date_difference, 6), [date_difference + 1])]

        def __tmp(x):
            x[1] -= datetime.timedelta(1)
            return x

        time_range = map(__tmp, [time_range[i:i + 2] for i in range(len(time_range) - 1)])

        index_tuple_ = ('Index',)
        qnalist_tuple = ('ItemID', 'ItemName', 'ListingImgUrl', 'VIPImgUrl', 'SellerID', 'QuestionNo', 'OrderNo',
                         'WriterID', 'Title', 'Content', 'Regdate', 'QueryType', 'AnsweredState')
        answers_tuple = ('QuestionNo', 'AnswerNo', 'WriterID', 'Title', 'Content', 'Regdate', 'NickName')

        index_tuple = tuple(enumerate(index_tuple_ + qnalist_tuple))
        index2_tuple = tuple(enumerate(answers_tuple, len(index_tuple)))

        sum_tuple = index_tuple_ + qnalist_tuple + answers_tuple

        length = len(index_tuple) + len(index2_tuple)
        write_list = ['' for _ in range(length)]
        idx = 0
        write_list[0] = str(idx)
        dict_ = defaultdict(list)

        row_num = 0
        xlsx_path = Auction.get_excel_path('cscollect')
        workbook = xlsxwriter.Workbook(xlsx_path, {'constant_memory': True})
        col_format = workbook.add_format({'bold': True})
        worksheet = workbook.add_worksheet()
        Auction._excel_write_row_const_memory(row_num, [col_map.get(i, i) for i in sum_tuple], worksheet, col_format)

        for s_time, e_time in time_range:
            sleep(Auction.SLEEP_TIME)
            body_format_dict['date1'] = s_time.strftime('%Y-%m-%d')
            body_format_dict['date2'] = e_time.strftime('%Y-%m-%d')
            body_f = body.format(**body_format_dict)
            result = get_soap(Auction.URL, Auction.HEADERS, body_f)
            result.raw.decode_content = True
            note = ET.iterparse(result.raw)

            for event, elem in note:
                if event == 'end':
                    if elem.tag.endswith('Answers'):
                        for k, v in elem.attrib.items():
                            dict_[k].append(v.strip())
                    elif elem.tag.endswith('QnAList'):
                        for index, key in index_tuple:
                            write_list[index] = elem.attrib.get(key, "").strip()
                        l_of_attrib = len(list(dict_.values())[0])
                        for ii in range(l_of_attrib):
                            for index, key in index2_tuple:
                                queryed = dict_.get(key, None)
                                if queryed is None:
                                    continue

                                queryed = queryed[ii].strip()
                                write_list[index] = queryed

                            idx += 1
                            write_list[0] = str(idx)
                            row_num += 1
                            Auction._excel_write_row_const_memory(row_num, write_list, worksheet)
                            dict_ = defaultdict(list)
                elem.clear()
        workbook.close()





    @staticmethod
    def _excel_write_row_const_memory(row_num, row, work_sheet, cell_format=None):
        for col in range(len(row)):
            work_sheet.write(row_num, col, row[col], cell_format)

    @staticmethod
    def _make_into_excel_time_limit_case(body, body_format_dict, start_time, end_time, time_seq, sum_tuple, xlsx_name,
                                         write_crit, end_crit, col_map={}):
        date_difference = (end_time-start_time).days
        time_range = [start_time + datetime.timedelta(x) for x in chain(range(0, date_difference, time_seq),
                                                                        [date_difference+1])]

        def __tmp(x):
            x[1] -= datetime.timedelta(1)
            return x

        time_range = map(__tmp, [time_range[i:i+2] for i in range(len(time_range)-1)])

        index_tuple = tuple(enumerate(sum_tuple))
        length = len(index_tuple)

        write_list = ['' for _ in range(length)]
        idx = 0
        write_list[0] = str(idx)

        row_num = 0
        workbook = xlsxwriter.Workbook(xlsx_name, {'constant_memory': True})
        col_format = workbook.add_format({'bold': True})
        worksheet = workbook.add_worksheet()

        Auction._excel_write_row_const_memory(row_num, [col_map.get(i, i) for i in sum_tuple], worksheet, col_format)

        dict_ = {}
        for s_time, e_time in time_range:
            try:
                body_format_dict['date1'] = s_time.strftime('%Y-%m-%d')
                body_format_dict['date2'] = e_time.strftime('%Y-%m-%d')
            except KeyError:
                sys.exit('date1 and date2 must be in key')
            body_f = body.format(**body_format_dict)
            result = get_soap(Auction.URL, Auction.HEADERS, body_f)
            result.raw.decode_content = True
            note = ET.iterparse(result.raw)

            # Sleep between query
            sleep(Auction.SLEEP_TIME)

            for event, elem in note:
                if event == 'end':

                    if elem.tag.endswith(write_crit):
                        dict_ = dict(dict_, **elem.attrib)
                    elif elem.tag.endswith(end_crit):
                        dict_ = dict(dict_, **elem.attrib)
                        for index, key in index_tuple:
                            write_list[index] = dict_.get(key, "").strip()

                        idx += 1
                        write_list[0] = str(idx)

                        row_num += 1
                        Auction._excel_write_row_const_memory(row_num, write_list, worksheet)

                        write_list = ['' for _ in range(length)]
                        dict_ = {}
                elem.clear()
        workbook.close()


# Auction.get_order_canceled_list(datetime.datetime(2017,5,5), datetime.datetime(2017,10,10))
# Auction.get_return_list(datetime.datetime(2017,5,5), datetime.datetime(2018,8,9)))
# Auction.get_paid_order_list(datetime.datetime(2017,5,5), datetime.datetime(2017,10,10))
# Auction.get_selling_item_list()
# Auction.get_settlement_list(datetime.datetime(2017,5,5), datetime.datetime(2017,10,10))
# Auction.get_seller_qna_list(datetime.datetime(2017,5,5), datetime.datetime(2017,10,10))

# print(ET.fromstring("""<?xml version="1.0" encoding="utf-8"?>
# <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
#   <soap:Header>
#     <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
#       <Value>string</Value>
#     </EncryptedTicket>
#   </soap:Header>
#   <soap:Body>
#     <DoShippingGeneralResponse xmlns="http://www.auction.co.kr/APIv1/AuctionService">
#       <DoShippingGeneralResult DoShippingGeneralResponseType="Success or Fail" FailReason="string" />
#     </DoShippingGeneralResponse>
#   </soap:Body>
# </soap:Envelope>""")[1][0][0].attrib)

# aa = ET.fromstring("""<?xml version="1.0" encoding="utf-8"?>
# <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
#   <soap:Header>
#     <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
#       <Value>string</Value>
#     </EncryptedTicket>
#   </soap:Header>
#   <soap:Body>
#     <AddQnAReplyResponse xmlns="http://www.auction.co.kr/APIv1/AuctionService">
#       <AddQnAReplyResult Status="boolean" Reason="string" AnswerNo="int" />
#     </AddQnAReplyResponse>
#   </soap:Body>
# </soap:Envelope>""")[1][0][0].attrib
# print(aa)

# aa = ET.fromstring("""<?xml version="1.0" encoding="utf-8"?>
# <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
#   <soap:Header>
#     <EncryptedTicket xmlns="http://www.auction.co.kr/Security">
#       <Value>d4rFDJHQMCGjKWaiIdSfsVs15OV60VoLJxwNt2tQIKXt6VD8K0SOXBePfcwjuUAEGyWoGhleC6hejU++HXrsemYsYeHq1W4tjiUKD3Cr69Z0PpS+xL3muGIW3ruPsS2kG/sPDPUpQAaoxMb5pzsWE6iDp/nypkLOhr1S2lj7eQFgFyQJ0WgNC/l6CpFrJaRI9Q5164WtWPku0wAh3icQ2JE=</Value>
#     </EncryptedTicket>
#   </soap:Header>
#   <soap:Body>
#     <GetMyAccountResponse xmlns="http://www.auction.co.kr/APIv1/AuctionService">
#       <GetMyAccountResult>
#         <MyAccount BankCode="1503" BankName="기업은행" AccountNumber="48004403104031" AccountName="(주)더싸다구" xmlns="http://schema.auction.co.kr/Arche.APISvc.xsd" />
#       </GetMyAccountResult>
#     </GetMyAccountResponse>
#   </soap:Body>
# </soap:Envelope>""")[1][0][0][0].attrib['AccoutName']
# print(aa)
#
