import sys
import os
import requests

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

from mapper import Mapper
from Makeshop.makeshop import MakeShop as m


class MakeShopMapper(Mapper):
    @classmethod
    def get_info_from_url(cls):
        # Shopping mall url도 받아와야함
        url = f"http://goup.finejin.com/ADMIN/api/user_conf.php?UserSeq={cls.args.u}&ServiceShopSeq={cls.args.ss}"
        js_on = requests.get(url).json()
        return js_on['Key']['id'], js_on['Key']['passwd'], js_on['UserName'], js_on['Key']['url'], js_on['Key']['vendor_id']


mapperclass = MakeShopMapper()
mapperclass.initiate()
id_, pwd, uname ,url, shop_url = mapperclass.get_info_from_url()


n = m(mapperclass.args.u, mapperclass.args.ss, id_, pwd, url)
n.login(shop_url)


if uname != n.get_username():
    raise Exception('FATAL ERROR!!! WRONG USER')

mapperclass.add_fun('item', n.item)
mapperclass.add_fun('paid', n.paid)
mapperclass.add_fun('cancel', n.cancel)
mapperclass.add_fun('orderreturn', n.orderreturn)
mapperclass.add_quiter(n.quit)

mapperclass.run()
