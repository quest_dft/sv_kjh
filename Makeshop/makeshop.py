import sys
import os
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select


sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

from seleniumcrawler import SeleniumCrawler
from time import sleep
from utilty import Util


class MakeShop(SeleniumCrawler):
    def login(self, shop_url):
        self.driver.implicitly_wait(1)
        self.driver.switch_to.frame(self.driver.find_element_by_css_selector('html > frameset > frame:nth-child(1)'))

        self.driver.implicitly_wait(1)
        self.driver.find_element_by_css_selector('#main-top > div.login-wrap > div.login-box > ul > li:nth-child(2) > a').click()
        self.driver.implicitly_wait(1)

        self.driver.find_element_by_xpath('//*[@id="login2Wrap"]/form[1]/fieldset/ul/li[1]/input').send_keys(shop_url)
        self.driver.find_element_by_css_selector('#login2Wrap > form:nth-child(1) > fieldset > ul > li:nth-child(2) > input').send_keys(self.id)
        self.driver.find_element_by_xpath('//*[@id="login2Wrap"]/form[1]/fieldset/ul/li[3]/input').send_keys(self.password)
        self.driver.implicitly_wait(2)

        self.driver.find_element_by_css_selector('#login2Wrap > form:nth-child(1) > fieldset > button').click()
        WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.XPATH, '//*[@id="main"]')))
        self.driver.implicitly_wait(2)
        # Take ScreenShot of login
        self.save_login_image()

    def get_username(self):
        self.driver.switch_to.frame(self.driver.find_element_by_id('topframe'))
        self.driver.implicitly_wait(1)

        tmp = self.driver.find_element_by_xpath('//*[@id="gnb"]/div[1]/div/div/div[1]/div[1]/div/span[1]').get_attribute('innerHTML')

        self.driver.switch_to.default_content()
        self.driver.implicitly_wait(1)

        return tmp

    @Util.Report
    def paid(self):
        self.driver.switch_to.frame(self.driver.find_element_by_id('topframe'))
        self.driver.implicitly_wait(1)

        self.driver.find_element_by_xpath('//*[@id="gnb"]/div[2]/div/div/ul/li[4]/a').click()

        self.driver.implicitly_wait(5)

        # Key Function
        self.driver.switch_to.default_content()
        self.driver.implicitly_wait(4)

        self.driver.switch_to_frame(1)
        self.driver.implicitly_wait(3)

        # 기간: 3일
        self.driver.find_element_by_css_selector('#termButtonID2').click()
        self.driver.implicitly_wait(3)

        self.driver.find_element_by_xpath('//*[@id="searchButton"]').click()
        WebDriverWait(self.driver, 7).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#orderNum')))
        self.driver.implicitly_wait(5)

        Select(self.driver.find_element_by_id("excel_formlist")).select_by_visible_text(u"전체")
        self.driver.implicitly_wait(2)

        self.enable_download()
        file_list = self._get_files_in_save_dir()

        # print(self.driver.find_element_by_css_selector())
        # self.driver.find_element_by_xpath('//*[@id="orderManager"]/div[3]/div[2]/fieldset/div[9]/p/span[3]/a').click()
        self.driver.find_element_by_link_text(u"엑셀다운로드").click()
        # self.driver.execute_script("chkSelectedList('chkItem','down')")


        tmp = False
        try:
            tmp = self._close_alert_and_get_its_text() == u'검색된 결과가 없습니다.'
        except Exception:
            pass
        finally:
            if not tmp:
                self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'csv'), 'csv')

        return self.message_list

    @Util.Report
    def cancel(self):
        self.driver.switch_to.frame(self.driver.find_element_by_id('topframe'))
        self.driver.implicitly_wait(1)

        self.driver.find_element_by_xpath('//*[@id="gnb"]/div[2]/div/div/ul/li[4]/a').click()
        self.driver.implicitly_wait(3)

        # Key Function
        self.driver.switch_to.default_content()
        self.driver.implicitly_wait(4)

        self.driver.switch_to_frame(1)
        self.driver.implicitly_wait(3)

        self.driver.find_element_by_id("M00030032_H3").click()
        self.driver.find_element_by_id("M000300320003").click()

        self.driver.implicitly_wait(5)

        # 기간: 3일
        self.driver.find_element_by_css_selector('#termButtonID2').click()
        self.driver.implicitly_wait(3)
        # 검색
        self.driver.find_element_by_id("searchButton").click()
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="listPNL"][contains(@style, "1")]')))
        self.driver.implicitly_wait(3)

        self.enable_download()
        file_list = self._get_files_in_save_dir()

        self.driver.find_element_by_link_text(u"엑셀다운로드").click()
        # self.driver.execute_script("chkSelectedList('chkItem','down')")

        tmp = False
        try:
            tmp = self._close_alert_and_get_its_text() == u'검색된 결과가 없습니다.'
        except Exception:
            pass
        finally:
            if not tmp:
                self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'csv'), 'csv')

        return self.message_list

    @Util.Report
    def orderreturn(self):

        self.driver.switch_to.frame(self.driver.find_element_by_id('topframe'))
        self.driver.implicitly_wait(1)

        self.driver.find_element_by_xpath('//*[@id="gnb"]/div[2]/div/div/ul/li[4]/a').click()
        self.driver.implicitly_wait(3)

        # Key Function
        self.driver.switch_to.default_content()
        self.driver.implicitly_wait(4)

        self.driver.switch_to_frame(1)
        self.driver.implicitly_wait(3)

        self.driver.find_element_by_id("M00030032_H3").click()
        self.driver.find_element_by_id("M000300320005").click()

        self.driver.implicitly_wait(5)

        # 기간: 3일
        self.driver.find_element_by_css_selector('#termButtonID2').click()
        self.driver.implicitly_wait(3)
        # 검색
        self.driver.find_element_by_id("searchButton").click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="listPNL"][contains(@style, "1")]')))
        self.driver.implicitly_wait(3)

        self.enable_download()
        file_list = self._get_files_in_save_dir()

        self.driver.find_element_by_link_text(u"엑셀다운로드").click()
        # self.driver.execute_script("chkSelectedList('chkItem','down')")

        tmp = False
        try:
            tmp = self._close_alert_and_get_its_text() == u'검색된 결과가 없습니다.'
        except Exception:
            pass
        finally:
            if not tmp:
                self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'csv'), 'csv')
        return self.message_list

    @Util.Report
    def item(self):

        self.driver.switch_to.frame(self.driver.find_element_by_id('topframe'))
        self.driver.implicitly_wait(1)

        self.driver.find_element_by_xpath('//*[@id="gnb"]/div[2]/div/div/ul/li[3]/a').click()
        self.driver.implicitly_wait(3)

        # Key Function
        self.driver.switch_to.default_content()
        self.driver.implicitly_wait(4)

        self.driver.switch_to_frame(1)
        self.driver.implicitly_wait(3)

        self.driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='진열'])[1]/input[1]").click()
        self.driver.find_element_by_xpath(
            u"(.//*[normalize-space(text()) and normalize-space(.)='오늘'])[1]/following::button[3]").click()
        self.driver.implicitly_wait(3)
        self._save_image('ITEM')

        self.driver.find_element_by_css_selector('#product_list > fieldset > div.btn-search-submit > input[type="image"]').click()

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="loading"][contains(@style, "none")]')))
        self.driver.implicitly_wait(4)

        self.enable_download()
        file_list = self._get_files_in_save_dir()

        self.driver.find_element_by_xpath('//*[@id="excel_download"]/img').click()
        self._close_alert_and_get_its_text()

        tmp = False
        try:
            tmp = self._close_alert_and_get_its_text() == u'검색된 결과가 없습니다.'
        except Exception:
            pass
        finally:
            if not tmp:
                self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xml'), 'xml')
        return self.message_list




#
# m = MakeShop(1002, 1003, 'easyplay', 'Tkekrn113@', "https://www.makeshop.co.kr/")
# m.login()
# m.get_username()
# m.item()