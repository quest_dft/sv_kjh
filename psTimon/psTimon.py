import sys
import os
import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from time import sleep

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

import seleniumcrawler
from utilty import Util


class PsTimon(seleniumcrawler.SeleniumCrawler):
    def login(self, cookie_url):
        cookies = self.cookie_check()
        if cookies is None:
            self.driver.find_element_by_id('loginId').send_keys(self.id)
            self.driver.find_element_by_css_selector('#txtPassword').send_keys(self.password)
            self.driver.find_element_by_css_selector('#frmLogin > fieldset > button').click()

            try:
                WebDriverWait(self.driver, 6).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#container > div > div.btn_area > a.btn_delay_modify')))
                self.driver.find_element_by_css_selector('#container > div > div.btn_area > a.btn_delay_modify').click()
            except Exception:
                pass

            WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body')))
            sleep(3)
        else:
            for i in cookies:
                self.driver.add_cookie(i)
            self.driver.get(cookie_url)
            self.driver.implicitly_wait(2)
            sleep(2)
            self.put_message('Cookie Login')

            #
        self.save_cookie()

        self.put_message('Login Successful')
        self.save_login_image()

    def get_username(self):
        return self.driver.find_element_by_css_selector('#header > div > span.name').get_attribute('title')

    @Util.Report
    def cscollect(self):
        self.driver.find_element_by_css_selector('#container > div.spot > ul > li:nth-child(4) > a').click()
        self.driver.implicitly_wait(1)
        self.driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='딜 별 조회'])[1]/following::button[1]").click()
        self.driver.find_element_by_id("DALL").click()
        self.driver.find_element_by_css_selector('#btnSearch').click()

        WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#divDetail > div > div.lst > table')))
        sleep(1)

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_css_selector('#btnExcel').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xls'), 'xls')

        return self.message_list

    @Util.Report
    def settlement(self):
        self.driver.find_element_by_css_selector('#container > div.spot > ul > li:nth-child(5) > a').click()
        self.driver.implicitly_wait(1)
        self.driver.find_element_by_css_selector('#container > div.spot > div.lnb_inner > ul > li.last > a').click()

        WebDriverWait(self.driver, 2).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#srch_start_month > option:nth-child(2)')))
        qq = self.driver.find_element_by_css_selector('#srch_start_month > option:nth-child(3)').get_attribute('innerHTML')
        Select(self.driver.find_element_by_id("srch_start_month")).select_by_visible_text(qq)

        self.driver.find_element_by_css_selector('#btn_sum').click()
        WebDriverWait(self.driver, 4).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#divSumList > div.sales_month_history')))
        self._save_image('POPOPOPOOPOPPO')

        self.enable_download()
        file_list = self._get_files_in_save_dir()

        self.driver.find_element_by_css_selector('#btn_sum_excel').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'monthlySalesSumExcel'), 'csv')
        return self.message_list


m = PsTimon(1008, 1012, u'더싸다구', 'dnjfaocnf10djr**', 'https://ps.ticketmonster.co.kr/')
m.login('https://ps.ticketmonster.co.kr/daily/')
print(m.get_username())
m.settlement()
# m.cscollect()