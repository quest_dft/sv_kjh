import argparse
from collections import defaultdict
import requests


class Mapper:
    # Extend this class and use when making ${shopseq}_args.py

    @staticmethod
    def pass_fun(*args):
        pass

    map = defaultdict(lambda: Mapper.pass_fun)
    quiter = None
    args = None

    @classmethod
    def initiate(cls):
        parser = argparse.ArgumentParser()

        parser.add_argument('-u', type=str)
        parser.add_argument('-ss', type=str)

        group = parser.add_mutually_exclusive_group()

        group.add_argument("-i", "--item", action="store_true")
        group.add_argument("-p", "--paid", action="store_true")
        group.add_argument("-c", "--cancel", action="store_true")
        group.add_argument("-r", "--orderreturn", action="store_true")
        group.add_argument("-csc", "--cscollect", action="store_true")
        group.add_argument("-s", "--settlement", action="store_true")
        group.add_argument("-css", "--cssend", action="store_true")
        group.add_argument("-sv", "--sendinvoice", action="store_true")

        parser.add_argument('args', nargs='*')
        cls.args = parser.parse_args()

    @classmethod
    def run(cls):
        args = cls.arg_parse_fun(cls.args.args)
        try:
            for k, v in vars(cls.args).items():
                if type(v) is bool and v:
                    cls.map[k](*args)
                    break
        except Exception as e:
            print(e)
        finally:
            cls.quiter()

    @classmethod
    def arg_parse_fun(cls, args):
        # Override and use it
        return args

    @classmethod
    def add_fun(cls, category, fun):
        cls.map[category] = fun

    @classmethod
    def add_quiter(cls, fun):
        cls.quiter = fun

    @classmethod
    def get_info_from_url(cls):
        raise NotImplementedError('Must override this method, or use other made method')

    @classmethod
    def get_info_from_url_id_pwd(cls):
        url = f"http://goup.finejin.com/ADMIN/api/user_conf.php?UserSeq={cls.args.u}&ServiceShopSeq={cls.args.ss}"
        js_on = requests.get(url).json()
        return js_on['Key']['id'], js_on['Key']['passwd'], js_on['UserName'], js_on['Key']['url'], js_on['ShopMainUrl'][js_on['ServiceMall']]




