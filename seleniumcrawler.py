from selenium import webdriver
from time import sleep
from glob import glob
import inspect
import datetime
from abc import abstractmethod
import os
from utilty import Util
import json


class SeleniumCrawler:
    ABS_PATH = os.path.dirname(os.path.abspath(__file__))
    DEFAULT_SAVE_DIR = ABS_PATH
    CHROMEDRIVER_FOLDER = os.path.join(ABS_PATH, 'bin')
    CHROMEDRIVER_PATH = os.path.join(CHROMEDRIVER_FOLDER, 'chromedriver')
    IMAGE_PATH = os.path.join(ABS_PATH, 'image_and_file', 'img')
    COOKIE_PATH = ABS_PATH

    if not os.path.exists(CHROMEDRIVER_FOLDER):
        os.makedirs(CHROMEDRIVER_FOLDER)
    if not os.path.exists(IMAGE_PATH):
        os.makedirs(IMAGE_PATH)

    def __init__(self, user, shopseq, id_, password, url):
        self.user = user
        self.shopseq = shopseq
        self.id = id_
        self.password = password
        __class__.change_save_dir(self.user, self.shopseq)
        self.save_dir = SeleniumCrawler.DEFAULT_SAVE_DIR

        self.message_list = []
        self.accept_next_alert = True
        self.url = url
        self._webdriver_connect()

    @classmethod
    def change_save_dir(cls, user, shopseq):

        cls.DEFAULT_SAVE_DIR = os.path.join(SeleniumCrawler.DEFAULT_SAVE_DIR, 'result/sujib/', f'{user}_{shopseq}')
        cls.COOKIE_PATH = os.path.join(SeleniumCrawler.COOKIE_PATH, 'cookie', f'{user}_{shopseq}.json')
        Util.change_save_dir(cls.DEFAULT_SAVE_DIR)
        if not os.path.exists(cls.DEFAULT_SAVE_DIR):
            os.makedirs(cls.DEFAULT_SAVE_DIR)

        if not os.path.exists(os.path.dirname(cls.COOKIE_PATH)):

            os.makedirs(os.path.dirname(cls.COOKIE_PATH))

    def cookie_check(self):
        try:
            with open(__class__.COOKIE_PATH, 'r') as f:
                data = json.load(f)

            cookie_time = datetime.datetime.strptime(data['time'], '%Y%m%d%H%M')
            current_time = self._get_current_time()
            if (current_time - cookie_time).total_seconds() > 1200:
                return None
            else:
                return data['cookies']
        except Exception:
            return None

    def save_cookie(self, *args):
        time = self._get_current_time().strftime('%Y%m%d%H%M')
        with open(__class__.COOKIE_PATH, 'w') as f:
            json.dump({'cookies': self.driver.get_cookies(), 'time': time}, f)

    def put_message(self, message):
        self.message_list.append(message)

    def _webdriver_connect(self):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('window-size=1920x1080')
        options.add_argument('disable-gpu')
        options.add_argument('disable-popup-blocking')

        # options.add_argument('--ignore-certificate-errors')
        # options.add_argument("--allow-http-screen-capture")
        # options.add_argument('--allow-insecure-localhost')
        # options.add_argument('--proxy-auto-detect')

        options.add_argument(
            "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")
        # options.add_argument('user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36')

        ####
        # options.add_experimental_option("prefs", {
        #     "download.prompt_for_download": False,
        #     "download.directory_upgrade": True,
        #     "safebrowsing.enabled": True,
        #     "download.default_directory": '/home/ubuntu/pycharm/SV/result'
        # })
        ###

        driver = webdriver.Chrome(SeleniumCrawler.CHROMEDRIVER_PATH, chrome_options=options)
        driver.get(self.url)
        self.driver = driver

        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)

    def enable_download(self):
        self.driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
        params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': self.save_dir}}
        self.driver.execute("send_command", params)

    def _get_files_in_save_dir(self):
        return set(os.listdir(self.save_dir))

    def _wait_until_download_is_complete(self, file_in_save_dir, end_):
        target_file = None
        while True:
            sleep(0.3)
            if target_file is None:
                tmp_set = self._get_files_in_save_dir() - file_in_save_dir
                target_file = None if len(tmp_set) is 0 else tuple(tmp_set)[0].split('.')[0]
            else:
                globed = os.path.basename(glob(os.path.join(self.save_dir, target_file+'*'))[0])
                if globed.endswith(end_):
                    return globed

    def _save_image(self, name):
        self.driver.get_screenshot_as_file(os.path.join(SeleniumCrawler.IMAGE_PATH,name+'.png'))

    def _change_name_of_file(self, filename, end ,resultname=None):
        filename = os.path.join(self.save_dir, filename)
        u_f_n = inspect.stack()[1][3] if resultname is None else inspect.stack()[1][3] + resultname
        c_d = self._get_current_time().strftime('%Y%m%d%H%M%S')
        new_filename = os.path.join(self.save_dir, f'{self.user}_{self.shopseq}_{u_f_n}_{c_d}.{end}')
        os.rename(filename, new_filename)

    def save_login_image(self):
        c_d = self._get_current_time().strftime('%Y%m%d%H%M%S')
        new_filename = os.path.join(self.save_dir, f'{self.user}_{self.shopseq}_login_{c_d}.png')
        self.driver.get_screenshot_as_file(new_filename)
        self.put_message({'Login success': new_filename})

    def _get_current_time(self):
        return datetime.datetime.utcnow() + datetime.timedelta(hours=9)

    def _close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def quit(self):
        self.driver.quit()

    @abstractmethod
    def login(self, *args):
        pass

    @abstractmethod
    def get_username(self):
        pass
