#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install python3.6
sudo apt-get install python3-pip
sudo pip3 install -r requirement.txt

# Korean
sudo apt-get install fonts-liberation libfontconfig
sudo apt-get install -y fonts-nanum*

# Install Chrome
sudo apt-get install libasound2 libnspr4 libnss3 xdg-utils
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt-get install libxss1 libgconf2-4 libappindicator1 libindicator7
sudo apt-get install libasound2 libnspr4 libnss3 xdg-utils
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt-get -f install
sudo dpkg -i google-chrome-stable_current_amd64.deb

wget https://chromedriver.storage.googleapis.com/2.43/chromedriver_linux64.zip
sudo apt-get install unzip
unzip chromedriver_linux64.zip


mv chromedriver bin/.
mv *.zip *.deb src

find -name "*.sh" -exec chmod +x {} \;

cat > test.py <<EOL
from selenium import webdriver
options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('window-size=1920x1080')
options.add_argument("disable-gpu")

import os
driver = webdriver.Chrome(os.path.join(os.getcwd(), 'chromedriver'), chrome_options=options)
driver.get('http://naver.com')
driver.implicitly_wait(3)
print(driver.get_screenshot_as_file('test.png'))
driver.quit()
EOL

echo "Try python3 test.py"
echo "If successfully installed, True will appear and test.png will be created"
