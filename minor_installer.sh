#!/usr/bin/env bash


option=${1:both}


function python_install(){
    pip3 install -r requirement.txt
}

function chromedriver_install(){
    wget https://chromedriver.storage.googleapis.com/2.43/chromedriver_linux64.zip
    sudo apt-get install unzip
    unzip chromedriver_linux64.zip
    mv chromedriver bin/.
    mv *.zip src
}


case ${option} in
python)
    python_install
    ;;
chromedriver)
    chromedriver_install
    ;;
both)
    python_install
    chromedriver_install
    ;;
*)
    echo Invalid Input
    ;;
esac

find -name "*.sh" -exec chmod +x {} \;




