import os
import json
import functools
import datetime

class Util:
    class Report:
        # Use as @Util.Report
        # When applying to staticmethod put @Util.Report under @staticmethod

        SAVE_DIR = os.getcwd()

        def __init__(self, f):
            self.f = f

        def __call__(self, *args, **kwargs):
            state, filename = False, ''
            file_list = Util.get_file_in_dir(Util.Report.SAVE_DIR)
            message = self.f(*args, **kwargs)
            file_list2 = tuple(Util.get_file_in_dir(Util.Report.SAVE_DIR) - file_list)
            if len(file_list2) != 0:
                state = True
                filename = []
                for i in file_list2:
                    filename.append(os.path.join(Util.Report.SAVE_DIR, i))
            print(json.dumps({'State': state, 'Filename': filename, 'Message': message}, indent=3, ensure_ascii=False))

            # Return state can be added

        def __get__(self, instance, instancetype):
            """Implement the descriptor protocol to make decorating instance
                method possible.
            """
            # https://stackoverflow.com/questions/5469956/python-decorator-self-is-mixed-up
            return functools.partial(self.__call__, instance)

    @staticmethod
    def get_file_in_dir(save_dir):
        return set(os.listdir(save_dir))

    @staticmethod
    def change_save_dir(save_dir):
        Util.Report.SAVE_DIR = save_dir


