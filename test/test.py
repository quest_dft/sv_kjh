import pprint
import requests
from time import sleep
import json
# url = f"http://goup.finejin.com/ADMIN/api/user_conf.php?UserSeq={1008}&ServiceShopSeq={1001}"
#
# js_on = requests.get(url).json()
#
# pprint.pprint(js_on)
#
# import os
# print(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)),
#                                                                   '..', 'image_and_file', 'img')))
import inspect
class A:
    def decorator_test(self, original_fun, t):
        error_msg = 'Must call this function as decorator!'
        assert any(line.startswith('@') for line in inspect.stack(context=2)[1].code_context), error_msg
        print(t)
        def wrapper(*args, **kwargs):
            print('Start')
            sleep(1)
            original_fun(*args, **kwargs)
            print('End')

        return wrapper

def f():
    print(inspect.stack()[1][3])


from functools import partial, wraps

def _pseudo_decor(fun, argument):
    # magic sauce to lift the name and doc of the function
    assert any(line.startswith('@') for line in inspect.stack(context=2)[1].code_context)
    @wraps(fun)
    def ret_fun(*args, **kwargs):
        #do stuff here, for eg.
        f()
        print ("decorator arg is %s" % str(argument))
        return fun(*args, **kwargs)
    return ret_fun

real_decorator = partial(_pseudo_decor, argument='e')

@real_decorator
def bar(test):
    f()
    pass

bar(3)

pprint.pprint({'a': 'qwqw', 'b': 'RRRR'}, width=1)
print(json.dumps({'a': 'qwqw', 'b': 'RRRR'}, indent=3))