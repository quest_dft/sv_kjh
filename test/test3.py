#!/usr/bin/env python

class PrintCallInfo:
    AA = 3
    def __init__(self,f):
        self.f = f
    def __call__(self,*args,**kwargs):
        print(PrintCallInfo.AA)
        r = self.f(*args,**kwargs)
        print('TEST FINISHED')


# the condition to modify the function...
some_condition=True

def my_decorator(f):
    if (some_condition): # modify the function
        return PrintCallInfo(f)
    else: # leave it as it is
        return f

@my_decorator
def foo():
    print( "foo")


foo()

foo()


