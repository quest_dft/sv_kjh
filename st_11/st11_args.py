import sys
import os

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

from mapper import Mapper
from st_11.st11 import St11 as s

class st11_mapper(Mapper):
    pass


mapperclass = st11_mapper()
mapperclass.initiate()
id_, pwd, uname ,url, url2 = mapperclass.get_info_from_url_id_pwd()

ss = s(mapperclass.args.u, mapperclass.args.ss, id_, pwd, url)
ss.login()

if uname not in ss.get_username():
    raise Exception('FATAL ERROR!!! WRONG USER')

mapperclass.add_fun('item', ss.item)
mapperclass.add_fun('cancel', ss.cancel)
mapperclass.add_fun('paid', ss.paid)
mapperclass.add_fun('orderreturn', ss.orderreturn)
mapperclass.add_fun('settlement', ss.settlement)
mapperclass.add_fun('cscollect', ss.cscollect)
mapperclass.add_quiter(ss.quit)

mapperclass.run()


