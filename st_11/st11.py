import sys
import os
import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from time import sleep
import inspect
import requests
import json

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

import seleniumcrawler
from utilty import Util


class St11(seleniumcrawler.SeleniumCrawler):
    def login(self):
        cookies, username = self.cookie_check()
        if cookies is None:
            self.driver.find_element_by_id('loginName').send_keys(self.id)
            self.driver.find_element_by_id('passWord').send_keys(self.password)
            self.driver.find_element_by_css_selector('#layBody > div > div > form > div.login_conts > div > div > fieldset > div > div > input').click()
            # WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#Content_ifrm_0')))
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'pageLoading')))
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="pageLoading"][contains(@style, "none")]')))
            self.driver.switch_to_frame(self.driver.find_element_by_id('Content_ifrm_0'))
            self.username = self.driver.find_element_by_css_selector('#ext-gen6 > div.mainWrap.renew_mobi > div.mainRignt > div.seller_rating_wrap > div.seller_rating_header > h3 > strong').get_attribute('innerHTML')

            self.save_cookie(self.username)
            self.put_message('Cookie Saved')
        else:
            for i in cookies:
                self.driver.add_cookie(i)
            self.username = username
            self.put_message('Cookie Login')
            #

        self.put_message('Login Successful')

    def get_username(self):
        return self.username

    def save_cookie(self, username):
        time = self._get_current_time().strftime('%Y%m%d%H%M')
        with open(__class__.COOKIE_PATH, 'w') as f:
            json.dump({'cookies': self.driver.get_cookies(), 'time': time, 'username': username}, f)

    def cookie_check(self):
        try:
            with open(__class__.COOKIE_PATH, 'r') as f:
                data = json.load(f)

            cookie_time = datetime.datetime.strptime(data['time'], '%Y%m%d%H%M')
            current_time = self._get_current_time()
            if (current_time - cookie_time).total_seconds() > 1200:
                return None, None
            else:
                return data['cookies'], data['username']
        except Exception:
            return None, None



    def get_name_of_file(self, end, resultname=None):
        u_f_n = inspect.stack()[1][3] if resultname is None else inspect.stack()[1][3] + resultname
        c_d = self._get_current_time().strftime('%Y%m%d%H%M%S')
        new_filename = os.path.join(self.save_dir, f'{self.user}_{self.shopseq}_{u_f_n}_{c_d}.{end}')
        return new_filename


    def get_size_of_dict(self, data):
        return str(sum(map(len, data.keys())) + sum(map(len, data.values())) + 2* len(data) - 1)


    @Util.Report
    def item(self):
        cookies = {}
        headers = {
            'Origin': 'http://soffice.11st.co.kr',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
            'Accept': 'text/plain, */*; q=0.01',
            'Referer': 'http://soffice.11st.co.kr/product/SellProductAction.tmall?method=getSellProductList',
            'X-Requested-With': 'XMLHttpRequest',
            'Proxy-Connection': 'keep-alive',
            'Content-Length': '860',
        }
        params = (
            ('method', 'getSellProductListJSON'),
            ('isExcel', 'Y'),
            ('isGlobalSeller', 'false'),
        )
        data = {
            'level': '',
            'currentPageNo': '1',
            'chkSelStatCds': '103',
            'chkPrdNoList': '',
            'chkPrdNoCount': '',
            'chkSelMthdList': '',
            'chkSelPrcList': '',
            'chkSoMaxCupnList': '',
            'chkDispCtgrNoList': '',
            'chkPrdNmList': '',
            'chkItemInfoList': '',
            'chkSelBgnDyList': '',
            'chkMobileYnList': '',
            'chkMobile1WonYnList': '',
            'chkDlvClfList': '',
            'bulkSellerYn': 'N',
            'searchListingItemClsf': '',
            'searchSumPremiumAplDt': '',
            'chkGblDlvYnList': '',
            'chkDlvCnntYnList': '',
            'chkPdEventCntList': '',
            'isFrame': 'N',
            'isTown': 'Y',
            'omPrdYn': '',
            'svcAreaCd': '',
            'isUseGlobalDeliver': 'Y',
            'shopNo': '',
            'selPrdTypCd': '',
            'chkPrdTypCdList': '',
            'chkSetTypCd': '',
            'prdNm': '',
            'prdNmTemp': '',
            'selStdPrdYn': '',
            'selMthdCd': '',
            'searchType': 'PRDNO',
            'prdNo': '',
            'prdNoTemp': '',
            'dlvClf': '',
            'selStatCd': '103',
            'category1': '',
            'category2': '',
            'category3': '',
            'category4': '',
            'dateType': 'CREATE',
            'createDt': '',
            'createDtTo': '',
            'sltDuration': 'ALL',
            'stckQty': '',
            'remainSelDt': '',
            'dlvCstPayTypCd': '',
            'dlvCstInstBasiCd': '',
            'premiumAplDt': '',
            'premiumPlusAplDt': '',
            'gifImageAplDt': '',
            'boldfaceAplDt': '',
            'bgColorAplDt': '',
            'adMovieDt': '',
            'mnbdClfCd': '',
            'mobilePrdYn': '',
            'engDispYn': '',
            'gblDlvYn': '',
            'dlvCnntYn': ''
        }

        for cookie in self.driver.get_cookies():
            cookies[cookie['name']] = cookie['value']

        response = requests.post('http://soffice.11st.co.kr/product/SellProductAjaxAction.tmall', headers=headers,
                                 params=params, cookies=cookies, data=data)
        with open(self.get_name_of_file('xls'), 'wb') as f:
            f.write(response.content)

        return self.message_list

    @Util.Report
    def paid(self, date1=None, date2=None):
        cur_time = self._get_current_time()
        if date1 is None:
            date1 = (cur_time - datetime.timedelta(days=30)).strftime('%Y/%m/%d')
        if date2 is None:
            date2 = cur_time.strftime('%Y/%m/%d')
        # Date formate must be %Y/%m/%d

        # self.driver.execute_script("javascript:goMenu('6206','https://soffice.11st.co.kr/escrow/SaleEndList.tmall','판매완료 내역'); rakeLog.sendRakeLog(this);")
        # WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'pageLoading')))
        # WebDriverWait(self.driver, 10).until(
        #         EC.presence_of_element_located((By.XPATH, '//*[@id="pageLoading"][contains(@style, "none")]')))
        # self.driver.switch_to_frame(self.driver.find_element_by_id('Content_ifrm_6206'))
        # self.driver.implicitly_wait(0.1)
        # self.driver.find_element_by_css_selector('#ext-gen7 > div.soWrap > div.sobunchW > div.sch_result_exArea > form > div > div.sch_box > div.btn_srh_area > div > button.defbtn_lar.ladtype.defbtn_seh').click()
        #
        # WebDriverWait(self.driver, 10).until(
        #     EC.presence_of_element_located((By.CSS_SELECTOR, '#dataGrid > div.jqx-rc-all[style*="none"]'))
        # )
        # self._save_image('11paid')
        cookies = {}
        headers = {
            'Origin': 'http://soffice.11st.co.kr',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Referer': 'https://soffice.11st.co.kr/escrow/SaleEndList.tmall?orgMenuNo=6206',
            'X-Requested-With': 'XMLHttpRequest',
            'Proxy-Connection': 'keep-alive',
            'Content-Length': '860',
        }

        data = {'excelDownType': 'newExcel', 'shBuyerType': '', 'shBuyerText': '', 'shGblDlv': '', 'addrSeq': '', 'shDateType': '01', 'shDateFrom': f'{date1}', 'shDateTo': f'{date2}', 'sltDuration': 'RECENT_MONTH', 'key': '901', 'listType': 'end', 'isItalyAgencyYn': ''}
        headers['Content-Length'] = self.get_size_of_dict(data)

        for cookie in self.driver.get_cookies():
            cookies[cookie['name']] = cookie['value']

        response = requests.post('https://soffice.11st.co.kr/escrow/SaleEndList.tmall?method=getSellListToExcel', headers=headers,
                                  cookies=cookies, data=data)
        with open(self.get_name_of_file('xls'), 'wb') as f:
            f.write(response.content)

        return self.message_list

    @Util.Report
    def orderreturn(self, returntype, date1=None, date2=None):
        # Default 30 days
        cur_time = self._get_current_time()
        if date1 is None:
            date1 = (cur_time - datetime.timedelta(days=30)).strftime('%Y/%m/%d')
        if date2 is None:
            date2 = cur_time.strftime('%Y/%m/%d')

        clmStat = '105' if returntype == '1' else '106'

        vis_string = '_request' if returntype == '1' else '_finished'

        cookies = {}
        headers = {
            'Origin': 'http://soffice.11st.co.kr',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Referer': 'https://soffice.11st.co.kr/escrow/AuthSellerClaimManager.tmall?method=getClaimList&clm=01&searchVer=02&orgMenuNo=6511',
            'X-Requested-With': 'XMLHttpRequest',
            'Proxy-Connection': 'keep-alive',
            'Content-Length': '860',
        }

        data = {'clmOccrTyp': '01', 'currentPageNo': '1', 'lastSearchKind': '02', 'smartSearchClmStat': '', 'searchVer': '02', 'townSellerYn': '', 'shDateFrom': '20181101', 'shDateTo': '20181130', 'key': 'searchALL', 'keyValue': '', 'clmStat': clmStat, 'shDateType': 'reqDt', 'startDate': f'{date1}', 'endDate': f'{date2}', 'sltDuration': 'LAST_MONTH'}
        headers['Content-Length'] = self.get_size_of_dict(data)

        for cookie in self.driver.get_cookies():
            cookies[cookie['name']] = cookie['value']

        response = requests.post('https://soffice.11st.co.kr/escrow/AuthSellerClaimManager.tmall?method=getClaimExcelList&shDlvClfCd=',
                                 headers=headers,
                                 cookies=cookies, data=data)
        with open(self.get_name_of_file('xls', vis_string), 'wb') as f:
            f.write(response.content)

        return self.message_list

    @Util.Report
    def cancel(self, canceltype, date1=None, date2=None):
        cur_time = self._get_current_time()
        if date1 is None:
            date1 = (cur_time - datetime.timedelta(days=30)).strftime('%Y/%m/%d')
        if date2 is None:
            date2 = cur_time.strftime('%Y/%m/%d')

        key = '01' if canceltype == '1' else '02'

        vis_string = '_request' if canceltype == '1' else '_finished'

        cookies = {}
        headers = {
            'Origin': 'http://soffice.11st.co.kr',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Referer': 'https://soffice.11st.co.kr/escrow/OrderCancelManageList2nd.tmall?orgMenuNo=6209',
            'X-Requested-With': 'XMLHttpRequest',
            'Proxy-Connection': 'keep-alive',
            'Content-Length': '860',
        }

        data = {'listType': 'cancel', 'pagePerSize': '500', 'currentPageNo': '1', 'ordPrdCnSeq': '', 'ordNoList': '',
                'ordPrdSeqList': '', 'ordPrdCnSeqList': '', 'searchFlag': '', 'shBuyerType': '', 'shBuyerText': '',
                'key': key, 'shDateType': '05', 'shDateFrom': f'{date1}', 'shDateTo': f'{date2}',
                'sltDuration': 'THIS_MONTH'}
        headers['Content-Length'] = self.get_size_of_dict(data)

        for cookie in self.driver.get_cookies():
            cookies[cookie['name']] = cookie['value']

        response = requests.post(
            'https://soffice.11st.co.kr/escrow/OrderCancelManageList.tmall?method=getSellListToExcel&ver=2nd&shDlvClfCd=',
            headers=headers,
            cookies=cookies, data=data)
        with open(self.get_name_of_file('xls', vis_string), 'wb') as f:
            f.write(response.content)

        return self.message_list

    @Util.Report
    def settlement(self, date1=None, date2=None):
        # Default 1day
        cur_time = self._get_current_time()
        if date1 is None:
            date1 = (cur_time - datetime.timedelta(days=1)).strftime('%Y/%m/%d')
        if date2 is None:
            date2 = cur_time.strftime('%Y/%m/%d')

        cookies = {}
        headers = {
            'Origin': 'http://soffice.11st.co.kr',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Referer': 'http://soffice.11st.co.kr/remittance/SellerRemittanceAction.tmall?method=displaySellerOwnSelStatSoffice',
            'X-Requested-With': 'XMLHttpRequest',
            'Proxy-Connection': 'keep-alive',
        }
        params = [('method', 'getSelStatAllDtlsExcelSoffice'), ('stlDy', ''), ('ordPrdStatExcel', ''),
                  ('unSpplRsrvtnDtls', ''), ('dtlSearchStlmntType', 'Y'), ('searchType', 'ALL'), ('intervalDay', '0'),
                  ('cnsgnDlvYn', 'N'), ('searchDiv', 'selStat'), ('searchPrd', 'date'),
                  ('searchDtType', 'BUY_CNFRM_DT'),
                  ('searchPrdDateCondition', 'YESTERDAY'), ('ordPrdStat', ''), ('dtlSearchType', ''),
                  ('dtlSearchVal', ''), ('sellState', 'ALL'), ('stDateTxt', date1), ('stDate', date1),
                  ('edDateTxt', date2), ('edDate', date2)]

        for cookie in self.driver.get_cookies():
            cookies[cookie['name']] = cookie['value']

        response = requests.get(
            'http://soffice.11st.co.kr/remittance/SellerRemittanceAction.tmall',
            headers=headers,
            cookies=cookies, params=params)
        with open(self.get_name_of_file('xls'), 'wb') as f:
            f.write(response.content)

        return self.message_list

    @Util.Report
    def cscollect(self, date1=None, date2=None):
        # Default 7days
        cur_time = self._get_current_time()
        if date1 is None:
            date1 = (cur_time - datetime.timedelta(days=7)).strftime('%Y/%m/%d')
        if date2 is None:
            date2 = cur_time.strftime('%Y/%m/%d')

        cookies = {}
        headers = {
            'Origin': 'http://soffice.11st.co.kr',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Referer': 'https://soffice.11st.co.kr/product/AuthUnityBoardAction.tmall?method=getProductQnaSellerList&orgMenuNo=8011',
            'X-Requested-With': 'XMLHttpRequest',
            'Proxy-Connection': 'keep-alive',
            'Content-Length': '860',
        }

        data = {'brdInfoNo': '', 'hgrnkBrdInfoNo': '', 'brdInfoClfNo': '', 'brdInfoCont': '', 'brdInfoSbjct': '', 'dispYn': '', 'rplyClf': '', 'method': 'getProductQnaSellerListExcel', 'buyerId': '', 'flag': 'sellerPrdQna', 'partFlag': 'seller', 'searchClsf': 'NORMAL', 'ntCodeType': '304', 'prdNoBox': '', 'sltTxtGubun': 'name', 'srchTxt': '', 'searchQnaDtlsCd': '', 'startDate': f'{date1}', 'endDate': f'{date2}', 'sltDuration': 'RECENT_WEEK', 'answerStatus': '', 'sltQnaTemp': '', 'answerCont': ''}
        headers['Content-Length'] = self.get_size_of_dict(data)

        for cookie in self.driver.get_cookies():
            cookies[cookie['name']] = cookie['value']

        response = requests.post(
            'https://soffice.11st.co.kr/product/AuthUnityBoardAction.tmall',
            headers=headers,
            cookies=cookies, data=data)
        with open(self.get_name_of_file('xls'), 'wb') as f:
            f.write(response.content)

        return self.message_list


# s = St11(1008, 1002, 'thessadagu', '11qjsrkdlatl12', 'http://soffice.11st.co.kr/Index.tmall')
# s.login()
# print('더싸다구' in s.get_username())
# s.cscollect()