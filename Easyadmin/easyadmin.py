import sys
import os
import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from time import sleep
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

import seleniumcrawler
from utilty import Util


class EasyAdmin(seleniumcrawler.SeleniumCrawler):
    def _webdriver_connect(self):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('window-size=1920x1080')

        options.add_argument('disable-popup-blocking')
        # options.add_argument("--disable-gpu")
        options.add_argument("lang=ko_KR")
        options.add_argument('--ignore-certificate-errors')

        options.add_argument(
            "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")

        caps = DesiredCapabilities.CHROME
        caps['loggingPrefs'] = {'performance': 'ALL'}
        # caps['acceptSslCerts'] = False
        caps['acceptInsecureCerts'] = True

        driver = webdriver.Chrome(seleniumcrawler.SeleniumCrawler.CHROMEDRIVER_PATH, chrome_options=options, desired_capabilities=caps)
        #
        driver.get('about:blank')
        driver.execute_script(
            "Object.defineProperty(navigator, 'plugins', {get: function() {return[1, 2, 3, 4, 5];},});")

        driver.get(self.url)
        self.driver = driver

        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)


    def login(self, shop_domain):
        # sleep(10)
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'login-domain')))
        a = self.driver.find_element_by_id('login-domain')
        a.clear()
        a.send_keys(shop_domain)
        a = self.driver.find_element_by_id('login-id')
        a.clear()
        a.send_keys(self.id)
        a = self.driver.find_element_by_id('login-pwd')
        a.clear()
        a.send_keys(self.password)

        sleep(1)
        self.driver.execute_script("document.querySelector('body > div.tiles > div:nth-child(1) > div > ul > div > form:nth-child(8) > div:nth-child(4) > a').click()")
        sleep(2)

        self.save_login_image()
        self.put_message('Login success')
        # cookies = self.cookie_check()
        # if cookies is None:
        #     self.driver.find_element_by_css_selector('#login_uid').send_keys(self.id)
        #     self.driver.find_element_by_css_selector('#login_upw_biz').send_keys(self.password)
        #     self.driver.find_element_by_css_selector('#loginConfirmBtn_biz').click()
        #     sleep(4)
        #
        #     WebDriverWait(self.driver, 4).until(EC.presence_of_element_located((By.CSS_SELECTOR,
        #                                                                         'body > div.layout.wrap > div.layout.container > div.layout.header > div > div > span > strong')))
        #     self.driver.find_element_by_css_selector('body > div.layout.wrap > div.layout.aside > h1 > a').click()
        #     WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.layout.wrap > div.layout.aside > div.partner-btn-area > div.wmp-partner-guide > button > span')))
        # else:
        #     for i in cookies:
        #         self.driver.add_cookie(i)
        #     self.driver.get(cookie_url)
        #     self.driver.implicitly_wait(2)
        #     sleep(2)
        #     self.put_message('Cookie Login')
        #
        #     #
        # self.save_cookie()
        #
        # self.put_message('Login Successful')
        # self.save_login_image()\
    @Util.Report
    def item(self, date1=None, date2=None):

        cur_time = self._get_current_time()
        if date1 is None:
            date1 = (cur_time - datetime.timedelta(days=7)).strftime('%Y-%m-%d')
        if date2 is None:
            date2 = cur_time.strftime('%Y-%m-%d')

        self.driver.execute_script("javascript:move_page25('C200');")
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#ptit02 > span.pgtitle')))
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'start_date')))
        self.driver.execute_script(f"document.querySelector('#start_date').value='{date1}'")
        self.driver.execute_script(f"document.querySelector('#end_date').value='{date2}'")
        self.driver.find_element_by_id("search").click()
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="load_grid1"][contains(@style, "none")]')))

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_css_selector('#download').click()
        try:
            text = self._close_alert_and_get_its_text()
            print(text)
        except Exception:
            return
        if text == u'다운로드 하시겠습니까?':
            print('F')
            self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xls'), 'xls')

        return self.message_list

tmp = 'http://admin40.ezadmin.co.kr/login.htm'
ww = EasyAdmin(1008, 1013, 'hnscom', 'hnscom1234**', tmp)
ww.login('thessadagu')
ww.item()
