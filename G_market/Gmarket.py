import os
import selenium.common.exceptions
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from time import sleep
from glob import glob
import inspect
import datetime
import sys

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))
from utilty import Util
from seleniumcrawler import  SeleniumCrawler


class Gmarket(SeleniumCrawler):
    def put_message(self, message):
        self.message_list.append(str(message))

    def _get_current_time(self):
        return datetime.datetime.utcnow() + datetime.timedelta(hours=9)

    def save_login_image(self):
        c_d = self._get_current_time().strftime('%Y%m%d%H%M%S')
        new_filename = os.path.join(self.save_dir, f'{self.user}_{self.shopseq}_login_{c_d}.png')
        self.driver.get_screenshot_as_file(new_filename)
        self.message_list.append({'Login Success': new_filename})

    def _webdriver_connect(self):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('window-size=1920x1080')
        options.add_argument('disable-gpu')
        options.add_argument(
            "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")
        driver = webdriver.Chrome(Gmarket.CHROMEDRIVER_PATH, chrome_options=options)
        driver.get(self.url)

        self.driver = driver
        self.driver.implicitly_wait(2)
        
        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)

    def login(self):
        self.driver.find_element_by_css_selector("input[type='radio'][value='GMKT']").click()

        self.driver.find_element_by_id('SiteId').send_keys(self.id)
        self.driver.find_element_by_id('SitePassword').send_keys(self.password)

        self.driver.execute_script("document.getElementById('btnSiteLogOn').click()")

        sleep(3.3)
        # Take ScreenShot of login
        self.save_login_image()
        self.put_message('Login successful')

    def get_username(self):

        return self.driver.find_element_by_xpath('//*[@id="header"]/div/span[1]/strong').get_attribute('innerHTML')

    def enable_download(self):
        self.driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
        params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': self.save_dir}}
        self.driver.execute("send_command", params)

    def _select_frame(self, tag_1, tag_2, frame_id, tag_3=None):
        elem = self.driver.find_element_by_class_name('like')
        id_num = elem.get_attribute('id')
        elem.click()
        wait = WebDriverWait(self.driver, 5)
        wait.until(EC.element_to_be_clickable((By.XPATH, f'//*[@id="{id_num}"]/span/a'))).click()
        sleep(1)

        if tag_3 is not None:
            wait2 = WebDriverWait(self.driver, 4)
            wait2.until(EC.element_to_be_clickable((By.XPATH, f'//*[@id="{tag_3}"]'))).click()
            self.driver.find_element_by_id(tag_3).click()
            self.driver.implicitly_wait(0.2)

        self.driver.find_element_by_id(tag_1).click()
        self.driver.implicitly_wait(0.1)
        self.driver.find_element_by_id(tag_2).click()
        self.driver.implicitly_wait(1)

        self.driver.switch_to.frame(self.driver.find_element_by_id(frame_id))
        self.driver.implicitly_wait(1.1)

    def _get_files_in_save_dir(self):
        return set(os.listdir(self.save_dir))

    def _wait_until_download_is_complete(self, file_in_save_dir):
        target_file = None
        while True:
            sleep(0.3)
            if target_file is None:
                tmp_set = self._get_files_in_save_dir() - file_in_save_dir
                target_file = None if len(tmp_set) is 0 else tuple(tmp_set)[0].split('.')[0]
            else:
                globed = os.path.basename(glob(os.path.join(self.save_dir, target_file+'.*'))[0])
                if globed.endswith('xls'):
                    return globed

    def _save_image(self, name):
        self.driver.get_screenshot_as_file(os.path.join(Gmarket.IMAGE_PATH, name+'.png'))

    def _change_name_of_file(self, filename, resultname=None):
        filename = os.path.join(self.save_dir, filename)
        u_f_n = inspect.stack()[1][3] if resultname is None else inspect.stack()[1][3] + resultname
        c_d = (datetime.datetime.utcnow() + datetime.timedelta(hours=9)).strftime('%Y%m%d%H%M%S')
        new_filename = os.path.join(self.save_dir, f'{self.user}_{self.shopseq}_{u_f_n}_{c_d}.xls')
        os.rename(filename, new_filename)

    def _change_name_of_file_multiple_case(self, filename, time, num, resultname=""):
        filename = os.path.join(self.save_dir, filename)
        u_f_n = inspect.stack()[1][3] if resultname is None else inspect.stack()[1][3] + resultname
        new_filename = os.path.join(self.save_dir, f'{self.user}_{self.shopseq}_{u_f_n}_{time}_{num}.xls')
        os.rename(filename, new_filename)


    def _close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    @Util.Report
    def item(self):
        self._select_frame('TDM001', 'TDM100', 'ifm_TDM100')

        self.driver.find_elements_by_name('chkSiteId')[2].click()
        self.driver.implicitly_wait(2)
        Select(self.driver.find_element_by_id("selPageSize")).select_by_visible_text(u"50개씩")

        try:
            WebDriverWait(self.driver, 10).until(
                EC.invisibility_of_element_located((By.CSS_SELECTOR, '#gridview-1055 div:nth-child(2)')))
        except selenium.common.exceptions.TimeoutException:
            self.put_message('No Item')
            return self.message_list
        self.enable_download()
        file_list = self._get_files_in_save_dir()

        total_number = int(self.driver.find_element_by_css_selector('#pagging_GSM001 > a:nth-last-child(3)').get_attribute('innerHTML'))
        time = self._get_current_time().strftime('%Y%m%d%H%M%S')
        for num in range(1, total_number+1):
            file_list = self._get_files_in_save_dir()
            self.driver.execute_script(f"javascript:pageClick({num}, 'GSM001');")
            WebDriverWait(self.driver, 10).until(
                EC.invisibility_of_element_located((By.CSS_SELECTOR, '#gridview-1055 div:nth-child(2)')))
            self.driver.find_element_by_id('aExcelDownload').click()

            file_name = self._wait_until_download_is_complete(file_list)
            self._change_name_of_file_multiple_case(file_name, time, num)


        #
        # self.driver.find_element_by_id('aExcelDownload').click()
        #
        # file_name = self._wait_until_download_is_complete(file_list)
        # self._change_name_of_file(file_name)

        return self.message_list

    @Util.Report
    def paid(self, date1, date2):
        # 신규주문
        self._select_frame('TDM002', 'TDM105', 'ifm_TDM105')
        Select(self.driver.find_element_by_id("searchAccount")).select_by_visible_text("G_"+self.id)
        # self.driver.find_element_by_id('searchSDT').send_keys('2018-10-05')
        # sdt = self.driver.find_element_by_id('searchSDT')
        # edt = self.driver.find_element_by_id('searchEDT')
        # self.driver.execute_script(f"document.getElementById('searchSDT').setAttribute('value','{date1}')")
        # self.driver.execute_script(f"document.getElementById('searchEDT').setAttribute('value','{date2}')")

        Select(self.driver.find_element_by_id("selPagingSize")).select_by_visible_text(u"50개씩")

        # self.driver.get_screenshot_as_file('QQQQQQQQQQQQQQQQQQQQ.png')
        #
        # # self.driver.execute_script('excelDownCase("no")')
        # self.driver.find_element_by_id("excelDown").click()
        # self._close_alert_and_get_its_text()
        # self.driver.switch_to.window(self.driver.window_handles[1])
        # self.driver.execute_script('ExcelSpiltCancel()')
        # # self._close_alert_and_get_its_text()
        # self.driver.switch_to.window(self.driver.window_handles[0])
        # self.driver.get_screenshot_as_file('order7.png')
        # self.driver.find_element_by_id("excelDown").click()
        # print(self._close_alert_and_get_its_text())
        #
        # self.driver.get_screenshot_as_file('order2.png')
        #
        #
        # self.driver.switch_to.window(self.driver.window_handles[1])
        # # print(self.driver.current_url)
        # # print(self.driver.page_source)
        #
        # async def test(driver):
        #     driver.find_element_by_xpath(
        #         '//*[@id="popContents"]/div/a[1]').click()
        #     print('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&')
        #     for entry in self.driver.get_log('browser'):
        #         print(entry)
        #
        # for entry in self.driver.get_log('browser'):
        #     print(entry)
        #
        # test(self.driver)
        self.driver.implicitly_wait(2)
        # print(self._close_alert_and_get_its_text())
        # print(self.driver.current_url)
        # print('clicked')
        # self.driver.get_screenshot_as_file('order3.png')
        self.driver.get_screenshot_as_file('order4_1.png')

        # print('clicked')
        print(self._close_alert_and_get_its_text())
        print('QQQQQQQQQQQQ')
        print(self.driver.current_url)
        self.driver.get_screenshot_as_file('order4.png')
        return self.message_list

    @Util.Report
    def cancel(self, cancel_type):
        self._select_frame('TDM003', 'TDM115', 'ifm_TDM115')
        Select(self.driver.find_element_by_id("searchAccount")).select_by_visible_text("G_"+self.id)

        vis_test = u'취소요청' if cancel_type == '1' else u'취소된거래'
        vis_string = '_request' if cancel_type == '1' else '_finished'
        Select(self.driver.find_element_by_id("searchType")).select_by_visible_text(vis_test)

        self.driver.find_element_by_link_text(u"1주일").click()
        Select(self.driver.find_element_by_id("selPagingSize")).select_by_visible_text(u"50개씩")
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="dataGrid"]/table/tbody/tr/td')))
        self.driver.find_element_by_id('btnSearch').click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="dataGrid"]/table/tbody/tr/td')))

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_xpath('//*[@id="contents"]/div[1]/form/div[4]/span/span[2]/a').click()
        self._close_alert_and_get_its_text()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list), vis_string)
        return self.message_list

    @Util.Report
    def orderreturn(self, returntype):
        self._select_frame('TDM003', 'TDM118', 'ifm_TDM118')
        Select(self.driver.find_element_by_id("searchAccount")).select_by_visible_text("G_"+self.id)
        Select(self.driver.find_element_by_id("searchDateType")).select_by_visible_text(u"반품신청일")
        vis_test = u'반품요청' if returntype == '1' else u'반품수거완료'
        vis_string = '_request' if returntype == '1' else '_finished'
        Select(self.driver.find_element_by_id("searchType")).select_by_visible_text(vis_test)

        self.driver.find_element_by_link_text(u"1주일").click()


        Select(self.driver.find_element_by_id("selPagingSize")).select_by_visible_text(u"50개씩")
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="dataGrid"]/table/tbody/tr/td')))
        self.driver.find_element_by_id('btnSearch').click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="dataGrid"]/table/tbody/tr/td')))

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_xpath('//*[@id="contents"]/div[1]/form/div[4]/span/span/a').click()
        self._close_alert_and_get_its_text()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list), vis_string)
        return self.message_list

    @Util.Report
    def settlement(self):
        self._select_frame('TDM004', 'TDM128', 'ifm_TDM128')
        self.driver.implicitly_wait(4)
        self.driver.find_element_by_css_selector(
            'body > div.settlement-report > div > div.ui-tabs > li:nth-child(2) > a').click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH,'//*[@id="Results"]')))
        self.driver.find_element_by_css_selector('#gmktSettlementReportPanel > div.account-search > fieldset > '
                                                 'span.date-set-group > span:nth-child(2) > span > button').click()
        self.driver.find_element_by_css_selector('#btnSearch').click()
        self.driver.implicitly_wait(0.5)
        WebDriverWait(self.driver, 10).until(
            EC.invisibility_of_element_located((By.XPATH, '//*[@class="loader"][contains(@style, "block")]')))

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_css_selector('#excelDown').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list))
        return self.message_list

    @Util.Report
    def cscollect(self):
        tag2 = 'TDM140'
        self._select_frame('TDM139', tag2, 'ifm_'+tag2, 'TDM007')
        # self._save_image('cscollect')

        self.driver.find_element_by_css_selector('#gmarket > a').click()
        WebDriverWait(self.driver, 3).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="gmarket"][@class="on"]')))

        # 1 week
        self.driver.find_element_by_css_selector('#contents > div.basic_contents > div.search_set > ul > li.first > '
                                                     'span > span:nth-child(2) > a').click()
        Select(self.driver.find_element_by_id("selPagingSize")).select_by_visible_text(u"50개씩")

        # Search
        self.driver.find_element_by_css_selector('#btnSearch').click()

        # Not done
        self.driver.implicitly_wait(10)

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_css_selector('#excelDown').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list))
        return self.message_list

    def quit(self):
        self.driver.quit()
