import Gmarket as G


import requests
import os
import argparse
import sys
import re
from dateutil.parser import parse
import datetime

parser = argparse.ArgumentParser()


parser.add_argument('-u', type=str)
parser.add_argument('-ss', type=str)

group = parser.add_mutually_exclusive_group()

group.add_argument("-i", "--item", action="store_true")
group.add_argument("-p", "--paid", action="store_true")
group.add_argument("-c", "--cancel", action="store_true")
group.add_argument("-r", "--orderreturn", action="store_true")
group.add_argument("-csc", "--cscollect", action="store_true")
group.add_argument("-s", "--settlement", action="store_true")
group.add_argument("-css", "--cssend", action="store_true")
group.add_argument("-sv", "--sendinvoice", action="store_true")

parser.add_argument('args', nargs='*')
args = parser.parse_args()

url = f"http://goup.finejin.com/ADMIN/api/user_conf.php?UserSeq={args.u}&ServiceShopSeq={args.ss}"

js_on = requests.get(url).json()
id_ = js_on['Key']['id']
password = js_on['Key']['passwd']
url_login = js_on['Key']['url']

g = G.Gmarket(args.u, args.ss,id_, password, url_login)
g.login()

if id_ != g.get_username():
    print(g.get_username())
    raise Exception('FATAL ERROR!!! WRONG USER')
try:
    if args.item:
        g.item()
    elif args.paid:
        pass
    elif args.cancel:
        g.cancel(*args.args)
    elif args.orderreturn:
        g.orderreturn(*args.args)
    elif args.cscollect:
        g.cscollect()
    elif args.settlement:
        g.settlement()
    elif args.cssend:
        pass
    elif args.sendinvoice:
        pass
except Exception as e:
    print(e)
finally:
    g.quit()