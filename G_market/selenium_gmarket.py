from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from bs4 import BeautifulSoup
import time

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('window-size=1920x1080')
options.add_argument('disable-gpu')
options.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")
# options.add_argument("download.default_directory=/home/ubuntu/pycharm/SV/G_market")
# prefs = {'download.prompt_for_download': False,
#          'download.directory_upgrade': True,
#          'safebrowsing.enabled': False,
#          'safebrowsing.disable_download_protection': True,
#          'download.default_directory': '/home/ubuntu/pycharm/SV/G_market',
#          'profile.default_content_setting_values.automatic_downloads': 2
#          }
# options.add_experimental_option('prefs',prefs)
# options.add_argument("lang=ko_KR") # 한국어!


def enable_download_in_headless_chrome(browser, download_dir):
    #add missing support for chrome "send_command"  to selenium webdriver
    browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')

    params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': download_dir}}
    browser.execute("send_command", params)




driver = webdriver.Chrome('/home/ubuntu/selenium/chromedriver', chrome_options=options)
driver.get('https://www.esmplus.com/Member/SignIn/LogOn?ReturnValue=-7')

driver.find_element_by_css_selector("input[type='radio'][value='GMKT']").click()

driver.find_element_by_id('SiteId').send_keys('thessadagu')
driver.find_element_by_id('SitePassword').send_keys('as15997353**')
driver.execute_script("document.getElementById('btnSiteLogOn').click()")

###################################################################################

print(driver.current_url)


def get_selling_item_list():
    print(driver.get_screenshot_as_file('selling_test_before.png'))
    elem = driver.find_element_by_class_name('like')
    id_num = elem.get_attribute('id')
    elem.click()
    wait = WebDriverWait(driver, 2)
    wait.until(EC.element_to_be_clickable((By.XPATH, f'//*[@id="{id_num}"]/span/a'))).click()

    driver.find_element_by_id('TDM001').click()
    driver.find_element_by_id('TDM100').click()
    driver.implicitly_wait(1)

    driver.switch_to.frame(driver.find_element_by_id('ifm_TDM100'))
    driver.find_elements_by_name('chkSiteId')[2].click()
    Select(driver.find_element_by_id("selPageSize")).select_by_visible_text(u"50개씩")

    # driver.find_element_by_id('imgItemsSearch').click()
    WebDriverWait(driver, 10).until(EC.invisibility_of_element_located((By.CSS_SELECTOR, '#gridview-1055 div:nth-child(2)')))
    # driver.get_screenshot_as_file('test.png')


    enable_download_in_headless_chrome(driver, '/home/ubuntu/pycharm/SV/G_market')
    #
    # print(driver.get_screenshot_as_file('selling_test_4_before.png'))
    driver.find_element_by_id('aExcelDownload').click()

    # WebDriverWait(driver,10)
    # time.sleep(3)
    driver.implicitly_wait(3)
    # print(driver.get_screenshot_as_file('selling_test_4_after.png'))

get_selling_item_list()


driver.quit()
