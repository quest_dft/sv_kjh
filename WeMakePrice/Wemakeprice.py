import sys
import os
import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from time import sleep

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

import seleniumcrawler
from utilty import Util


class Wemakeprice(seleniumcrawler.SeleniumCrawler):
    def login(self, cookie_url):
        cookies = self.cookie_check()
        if cookies is None:
            self.driver.find_element_by_css_selector('#login_uid').send_keys(self.id)
            self.driver.find_element_by_css_selector('#login_upw_biz').send_keys(self.password)
            self.driver.find_element_by_css_selector('#loginConfirmBtn_biz').click()
            sleep(4)

            WebDriverWait(self.driver, 4).until(EC.presence_of_element_located((By.CSS_SELECTOR,
                                                                                'body > div.layout.wrap > div.layout.container > div.layout.header > div > div > span > strong')))
            self.driver.find_element_by_css_selector('body > div.layout.wrap > div.layout.aside > h1 > a').click()
            WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.layout.wrap > div.layout.aside > div.partner-btn-area > div.wmp-partner-guide > button > span')))
        else:
            for i in cookies:
                self.driver.add_cookie(i)
            self.driver.get(cookie_url)
            self.driver.implicitly_wait(2)
            sleep(2)
            self.put_message('Cookie Login')

            #
        self.save_cookie()

        self.put_message('Login Successful')
        self.save_login_image()

        self.close_banner()

    def get_username(self):
        return self.driver.find_element_by_css_selector('body > div.layout.wrap > div.layout.container > div.layout.header > div > div > span > strong').get_attribute('innerHTML')

    def close_banner(self):
        for i in self.driver.find_elements_by_class_name('ban-close'):
            i.click()
            self.put_message('Banner Closed')
        for i in self.driver.find_elements_by_css_selector('a.popup_floatbtn'):
            i.click()
            self.put_message('Banner Closed')

    @Util.Report
    def item(self):
        self.driver.execute_script("document.querySelector('body > div.layout.wrap > div.layout.aside > ul > li:nth-child(2) > a').click()")
        self.driver.execute_script("document.querySelector('body > div.layout.wrap > div.layout.aside > ul > li.active > div > ul > li:nth-child(1) > a').click()")
        # Not yet implemented

        tmp = False
        try:
            tmp = self._close_alert_and_get_its_text() == u'현재 판매중인 딜이 없습니다.'

        except Exception:
            pass
        finally:
            # Cannot check excel download yes

            if not tmp:
                pass

        return self.message_list

    @Util.Report
    def settlement(self, date1=None, date2=None):
        cur_time = self._get_current_time()
        if date1 is None:
            date1 = (cur_time - datetime.timedelta(days=7)).strftime('%Y-%m-%d')
        if date2 is None:
            date2 = cur_time.strftime('%Y-%m-%d')

        self.driver.execute_script("document.querySelector('body > div.layout.wrap > div.layout.aside > ul > li:nth-child(3) > a').click()")
        self.driver.execute_script("document.querySelector('body > div.layout.wrap > div.layout.aside > ul > li.active > div > ul > li:nth-child(1) > a').click()")

        WebDriverWait(self.driver, 4).until(EC.presence_of_element_located((By.ID, 'date_start')))
        self.driver.execute_script(f"document.querySelector('#date_start').value='{date1}'")
        self.driver.execute_script(f"document.querySelector('#date_end').value='{date2}'")

        self.driver.find_element_by_css_selector('#btn_search').click()


        # WebDriverWait must be included
        sleep(1)

        # Download
        self.enable_download()
        file_list = self._get_files_in_save_dir()


        self.driver.find_element_by_css_selector('body > div.layout.wrap > div.layout.container > div.layout.contents > div > div:nth-child(1) > div.search_result > h3 > div > a.btn_fix.btn_exceldown').click()

        tmp = False
        try:
            tmp = self._close_alert_and_get_its_text() == u'검색 결과가 없습니다. 다시 시도해 주세요.'

        except Exception:
            pass
        finally:
            # Cannot check excel download yes

            if not tmp:
                pass

        return self.message_list

    @Util.Report
    def cancel(self):
        self.driver.execute_script("document.querySelector('body > div.layout.wrap > div.layout.aside > ul > li:nth-child(5) > a').click()")
        self.driver.execute_script("document.querySelector('body > div.layout.wrap > div.layout.aside > ul > li.active > div > ul > li:nth-child(2) > a').click()")

        self.driver.implicitly_wait(1)
        WebDriverWait(self.driver, 3).until(EC.element_to_be_clickable((By.ID, 'moretxt')))
        self.driver.find_element_by_id("moretxt").click()
        # 1 week
        self.driver.find_element_by_css_selector('#search_area > dl:nth-child(1) > dd > a:nth-child(4)').click()

        # Search
        self.driver.find_element_by_css_selector('#search_area > a > span').click()

        # Not yet implemented , html parsing
        return self.message_list

    @Util.Report
    def orderreturn(self):
        self.driver.execute_script(
            "document.querySelector('body > div.layout.wrap > div.layout.aside > ul > li:nth-child(5) > a').click()")
        self.driver.execute_script("document.querySelector('body > div.layout.wrap > div.layout.aside > ul > li.active > div > ul > li:nth-child(3) > a').click()")
        WebDriverWait(self.driver, 4).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'body > div.layout.wrap > div.layout.container > div.layout.contents > div:nth-child(1) > form > fieldset > div.search-option > dl:nth-child(1) > dd > div > div.duration-button > label:nth-child(3)')))
        self.driver.find_element_by_css_selector('body > div.layout.wrap > div.layout.container > div.layout.contents > div:nth-child(1) > form > fieldset > div.search-option > dl:nth-child(1) > dd > div > div.duration-button > label:nth-child(3)').click()
        self.driver.find_element_by_css_selector('#btn-find').click()
        WebDriverWait(self.driver, 5).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="processing_indicator"][contains(@style, "none")]')))

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_css_selector('#btn-excel').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xlsx'), 'xlsx')
        return self.message_list

    @Util.Report
    def cscollect(self):
        self.driver.execute_script("document.querySelector('body > div.layout.wrap > div.layout.aside > ul > li:nth-child(10) > a').click()")
        self.driver.implicitly_wait(1)
        WebDriverWait(self.driver, 3).until(EC.element_to_be_clickable((By.LINK_TEXT, u'전체24')))
        self.driver.find_element_by_link_text(u"전체24").click()

        WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#cnt_summary > fieldset:nth-child(3) > div > table > thead > tr > th:nth-child(1)')))
        sleep(1.4)

        # Download
        self.enable_download()
        file_list = self._get_files_in_save_dir()

        self.driver.find_element_by_css_selector('#cnt_summary > fieldset:nth-child(3) > div > div > span.option2 > a').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xlsx'), 'xlsx')

        return self.message_list



# m = Wemakeprice(1008, 1009, 'thessadagu', 'dnjf10djr**', "https://biz.wemakeprice.com/partner/login")
#
# m.login('http://biz.wemakeprice.com/dealer/dashboard')
#
# m.cscollect()