import sys
import os

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

from mapper import Mapper
from WeMakePrice.Wemakeprice import Wemakeprice as we

class WemakepriceMapper(Mapper):
    pass


mapperclass = WemakepriceMapper()
mapperclass.initiate()
id_, pwd, uname ,url, url2 = mapperclass.get_info_from_url_id_pwd()

w = we(mapperclass.args.u, mapperclass.args.ss, id_, pwd, url)
w.login(url2)

if uname not in w.get_username():
    raise Exception('FATAL ERROR!!! WRONG USER')

mapperclass.add_fun('item', w.item)
mapperclass.add_fun('cancel', w.cancel)
mapperclass.add_fun('orderreturn', w.orderreturn)
mapperclass.add_fun('settlement', w.settlement)
mapperclass.add_fun('cscollect', w.cscollect)
mapperclass.add_quiter(w.quit)

mapperclass.run()


