import sys
import os
import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from time import sleep
from dateutil.relativedelta import relativedelta
import json
from collections import OrderedDict
import xlsxwriter

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

import seleniumcrawler
from utilty import Util
url = "https://sell.smartstore.naver.com/#/login"


class NaverStore(seleniumcrawler.SeleniumCrawler):
    def login(self, cookie_url):
        cookies = self.cookie_check()
        if cookies is None:
            self.driver.implicitly_wait(1)
            self.driver.find_element_by_id('loginId').send_keys(self.id)
            self.driver.find_element_by_id('loginPassword').send_keys(self.password)
            self.driver.find_element_by_id('loginButton').click()
            WebDriverWait(self.driver, 2).until(EC.url_changes('https://sell.smartstore.naver.com/#/home/dashboard'))
            sleep(3)
            self.correct_manager_name()
        else:
            for i in cookies:
                self.driver.add_cookie(i)
            self.driver.get(cookie_url)
            self.driver.implicitly_wait(2)
            sleep(2)
            self.put_message('Cookie Login')

            #
        self.save_cookie()
        # Take ScreenShot of login
        self.save_login_image()
        self.put_message('Login successful')

    def get_username(self):
        return self.driver.find_element_by_css_selector('#_gnb_nav > ul > li:nth-child(1) > a').get_attribute('text').split()[0]

    def correct_manager_name(self):
        WebDriverWait(self.driver, 3).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#_gnb_nav > ul > li:nth-child(2) > a')))
        self.driver.find_element_by_css_selector('#_gnb_nav > ul > li:nth-child(2) > a').click()
        self.driver.implicitly_wait(0.8)
        html_tag = self.driver.find_elements_by_css_selector('div.select-area > div.seller-input')
        for tag in html_tag:
            name = tag.find_element_by_css_selector('label span.sub-text').get_attribute('innerHTML')
            if name == u'(계정 주매니저)':
                if tag.find_element_by_css_selector('label input').get_attribute('checked') is None:
                    self.driver.implicitly_wait(2)
                    tag.find_element_by_css_selector('label span').click()
                    sleep(4.5)
                    self.put_message('Manager name changed')
                    return
        self.driver.implicitly_wait(2)
        self.driver.find_element_by_css_selector('body > div.modal.model.fade.seller-layer-modal.data-target-channel-list.in > div > div > div.modal-header.bg-primary > button').click()
        sleep(2)

    @Util.Report
    def item(self):
        self.driver.implicitly_wait(1)
        self.driver.find_element_by_link_text(u"상품관리").click()
        self.driver.implicitly_wait(1)
        self.driver.find_element_by_link_text(u"상품 조회/수정").click()
        self.driver.implicitly_wait(0.5)
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable,
                                             self.driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and "
                                                                               u"normalize-space(.)='전체'])[2]/span[1]"))

        self.driver.implicitly_wait(2)
        self.driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='전체'])[2]/span[1]").click()

        self.driver.implicitly_wait(1)
        self.driver.find_element_by_css_selector('#seller-content > ui-view > div > ui-view:nth-child(1) > div.panel.panel-seller > form > div.panel-footer > div > button.btn.btn-primary').click()


        # alternative must be included
        sleep(2.3)

        self.enable_download()
        file_list = self._get_files_in_save_dir()

        self.driver.implicitly_wait(2)
        self.driver.find_element_by_xpath('//*[@id="seller-content"]/ui-view/div/ui-view[2]/div/div[1]/div[2]/div/'
                                          'div[2]/button/span[1]').click()


        tt = self.driver.find_element_by_css_selector('#seller-content > ui-view > div > ui-view:nth-child(2) > div > div.panel-heading > div.pull-left > h3 > span').get_attribute('innerHTML')
        self.put_message(f'{tt} items found')
        if tt != '0':
            self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'csv'), 'csv')

        return self.message_list

    @Util.Report
    def paid(self):
        self.driver.implicitly_wait(1)
        self.driver.find_element_by_link_text(u"판매관리").click()
        self.driver.implicitly_wait(0.5)
        self.driver.find_element_by_link_text(u"주문 조회").click()
        self.driver.implicitly_wait(1)

        self.driver.switch_to.frame(self.driver.find_element_by_id('__naverpay'))
        try:
            WebDriverWait(self.driver, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR,'body > div.npay_content._root > div.napy_sub_content > div:nth-child(2) > div.npay_grid_area > div.grid._detailGrid._grid_container.uio_grid > div._flexible_area.flexible_area > div._body.body > div._table_container > table > tbody > tr:nth-child(1)')))
        except Exception:
            pass

        self.driver.implicitly_wait(2)
        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[2]/div[1]/div/button').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xls'), 'xls')

        return self.message_list

    @Util.Report
    def cancel(self, canceltype):
        self.driver.implicitly_wait(0.8)
        self.driver.find_element_by_link_text(u"판매관리").click()
        self.driver.implicitly_wait(0.5)
        self.driver.find_element_by_link_text(u"취소 관리").click()
        self.driver.implicitly_wait(1)

        text = u'취소요청' if canceltype == '1' else u'취소완료'
        name = '_request' if canceltype == '1' else '_finished'
        self.driver.switch_to.frame(self.driver.find_element_by_id('__naverpay'))
        WebDriverWait(self.driver, 4).until(EC.presence_of_element_located((By.XPATH, '//*[@class="_loading_layer loading_layer"][contains(@style, "none")]')))
        self.driver.implicitly_wait(1)

        Select(self.driver.find_element_by_id("productOrder.lastClaim.claimStatus")).select_by_visible_text(text)
        self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[2]/div[2]/button[1]').click()
        WebDriverWait(self.driver, 4).until(EC.presence_of_element_located(
            (By.XPATH, '//*[@class="_loading_layer loading_layer"][contains(@style, "none")]')))
        self.driver.implicitly_wait(1)

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div[1]/div/button').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xls'), 'xls', name)

        return self.message_list

    @Util.Report
    def orderreturn(self, returntype):
        self.driver.implicitly_wait(0.8)
        self.driver.find_element_by_link_text(u"판매관리").click()
        self.driver.implicitly_wait(0.5)
        self.driver.find_element_by_link_text(u"반품 관리").click()
        self.driver.implicitly_wait(1)

        text = u'반품요청' if returntype == '1' else u'반품완료'
        name = '_request' if returntype == '1' else '_finished'
        self.driver.switch_to.frame(self.driver.find_element_by_id('__naverpay'))
        WebDriverWait(self.driver, 4).until(EC.presence_of_element_located(
            (By.XPATH, '//*[@class="_loading_layer loading_layer"][contains(@style, "none")]')))
        self.driver.implicitly_wait(1)

        Select(self.driver.find_element_by_id("productOrder.lastClaim.claimStatus")).select_by_visible_text(text)
        self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[2]/div[2]/button[1]').click()
        WebDriverWait(self.driver, 4).until(EC.presence_of_element_located(
            (By.XPATH, '//*[@class="_loading_layer loading_layer"][contains(@style, "none")]')))
        self.driver.implicitly_wait(1)

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div[1]/div/button').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xls'), 'xls', name)

        return self.message_list

    @Util.Report
    def settlement(self):
        self.driver.implicitly_wait(0.8)
        self.driver.find_element_by_link_text(u"정산관리").click()
        self.driver.implicitly_wait(0.5)
        self.driver.find_element_by_link_text(u"정산 내역").click()
        self.driver.implicitly_wait(1)
        self.driver.switch_to.frame(self.driver.find_element_by_id('__naverpay'))
        sleep(3)
        self.driver.find_element_by_xpath('//*[@id="startDate"]/button').click()
        self.driver.find_element_by_link_text("1").click()

        self.driver.find_element_by_xpath('//*[@id="searchForm"]/div[3]/button[1]').click()
        WebDriverWait(self.driver, 4).until(EC.presence_of_element_located(
            (By.XPATH, '//*[@class="_loading_layer"][contains(@style, "none")]')))
        self.driver.implicitly_wait(1)

        self._save_image('SETTLE2')

        self.enable_download()
        file_name = self._get_files_in_save_dir()
        self.driver.find_element_by_xpath('//*[@id="content"]/div/div[2]/div[3]/div[1]/div/button').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_name, 'xlsx'), 'xlsx')

        return self.message_list

    @Util.Report
    def cscollect(self):
        self.driver.implicitly_wait(0.8)
        self.driver.find_element_by_link_text(u"문의/리뷰관리").click()
        self.driver.implicitly_wait(0.5)
        self.driver.find_element_by_link_text(u"문의 관리").click()
        self.driver.implicitly_wait(1)
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[contains(@class, "pace-inactive")]')))
        self.driver.find_element_by_xpath(
            u"(.//*[normalize-space(text()) and normalize-space(.)='건'])[1]/following::div[2]").click()
        self.driver.find_element_by_xpath(
            u"(.//*[normalize-space(text()) and normalize-space(.)='건'])[1]/following::a[1]").click()

        self._save_image('TESTFINAL')

        self.driver.implicitly_wait(1)
        self.driver.find_element_by_xpath(
            u"(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/following::button[1]").click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//div[contains(@class, "pace-inactive")]')))

        total_num = int(self.driver.find_element_by_css_selector('#seller-content > ui-view > div > div > div.panel-heading > div.pull-left > h3 > span > strong').get_attribute('innerHTML'))
        enddate = (datetime.datetime.utcnow() + datetime.timedelta(hours=9)).strftime('%Y-%m-%d')
        startdate = (datetime.datetime.utcnow() + datetime.timedelta(hours=33) - relativedelta(months=3)).strftime('%Y-%m-%d')
        page = 0
        column_not_done = True
        key1 = None
        key2 = ('commentContent',)

        c_d = (datetime.datetime.utcnow() + datetime.timedelta(hours=9)).strftime('%Y%m%d%H%M%S')
        xlsx_name = os.path.join(self.save_dir, f'{self.user}_{self.shopseq}_cscollect_{c_d}.xlsx')

        workbook = xlsxwriter.Workbook(xlsx_name, {'constant_memory': True})
        col_format = workbook.add_format({'bold': True})
        worksheet = workbook.add_worksheet()
        row_num = 0

        while True:
            self.driver.get(f'https://sell.smartstore.naver.com/api/comment?_action=queryPage&commentType=&endDate={enddate}T23:59:59.999%2B09:00&keyword=&page={page}&range=5&searchKeywordType=PRODUCT_NAME&sellerAnswerYn=&size=8&startDate={startdate}T00:00:00.000%2B09:00&totalCount={total_num}')
            self.driver.implicitly_wait(1)
            self.driver.find_element_by_css_selector('body > pre').get_attribute('innerHTML')

            json1 = json.loads(self.driver.find_element_by_css_selector('body > pre').get_attribute('innerHTML'),
                               encoding='utf-8', object_pairs_hook=OrderedDict)
            if len(json1['content']) == 0:
                break
            if key1 is None:
                key1 = tuple(json1['content'][0].keys())
            for content in json1['content']:
                reply = ""

                if content['replyCnt'] == 1:
                    id_tmp = content['id']
                    self.driver.get(f'https://sell.smartstore.naver.com/api/comment/{id_tmp}')
                    self.driver.implicitly_wait(1)
                    json2 = json.loads(self.driver.find_element_by_css_selector('body > pre').get_attribute('innerHTML'),
                                       encoding='utf-8')
                    reply = str(json2[0]['commentContent'])

                if column_not_done:
                    tmptmp = key1 + key2
                    for col in range(len(tmptmp)):
                        worksheet.write(row_num, col, tmptmp[col], col_format)
                    row_num += 1
                    column_not_done = False
                values_ = tuple(map(str, content.values())) + (reply,)
                for col in range(len(values_)):
                    worksheet.write(row_num, col, values_[col])
                row_num += 1
            page += 1

        workbook.close()
        return self.message_list

        # print(self.driver.find_element_by_id('interceptedResponse').get_attribute('innerHTML'))
