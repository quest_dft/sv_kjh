import sys
import os

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

from mapper import Mapper
from NaverStore.naverstore import NaverStore as ns


class NaverStoreMapper(Mapper):
    pass


mapperclass = NaverStoreMapper()
mapperclass.initiate()
id_, pwd, uname ,url, url2 = mapperclass.get_info_from_url_id_pwd()


n = ns(mapperclass.args.u, mapperclass.args.ss, id_, pwd, url)
n.login(url2)

if id_ != n.get_username():
    raise Exception('FATAL ERROR!!! WRONG USER')

mapperclass.add_fun('item', n.item)
mapperclass.add_fun('paid', n.paid)
mapperclass.add_fun('cancel', n.cancel)
mapperclass.add_fun('orderreturn', n.orderreturn)
mapperclass.add_fun('settlement', n.settlement)
mapperclass.add_fun('cscollect', n.cscollect)
mapperclass.add_quiter(n.quit)

mapperclass.run()
