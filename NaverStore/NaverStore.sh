#!/usr/bin/env bash

# must change the file name to appropriate "shopseq.sh" ex) Auction.sh
# The mapper python file must be like "shopseq_args.py" ex) Auction_args.py

declare -A args=( ["-i"]="" ["-p"]="" ["-c"]="0"  ["-r"]="0" ["-csc"]="" ["-s"]="")
filename=`basename $0`
new_file=${filename%.*}
for args_k in ${!args[@]}
do
    echo ${args_k}
    python3 ${new_file}_args.py -u 1008 -ss 1008 ${args_k} ${args[$args_k]}
    echo -e 'Done\n'
done

declare -A args=( ["-r"]="1" ["-c"]="1")
for args_k in ${!args[@]}
do
    echo ${args_k}
    python3 ${new_file}_args.py -u 1008 -ss 1008 ${args_k} ${args[$args_k]}
    echo -e 'Done\n'
done