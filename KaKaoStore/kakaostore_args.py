import sys
import os

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

from mapper import Mapper
from KaKaoStore.kakaostore import KaKaoStore as ka

class KaKaoMapper(Mapper):
    pass

mapperclass = KaKaoMapper()
mapperclass.initiate()
id_, pwd, uname ,url, url2 = mapperclass.get_info_from_url_id_pwd()

k = ka(mapperclass.args.u, mapperclass.args.ss, id_, pwd, url)
k.login('https://store-sell.kakao.com/dashboard')

if uname != k.get_username():
    raise Exception('FATAL ERROR!!! WRONG USER')

mapperclass.add_fun('item', k.item)
mapperclass.add_fun('cancel', k.cancel)
mapperclass.add_fun('paid', k.paid)
mapperclass.add_fun('orderreturn', k.orderreturn)
mapperclass.add_fun('settlement', k.settlement)
mapperclass.add_fun('cscollect', k.cscollect)
mapperclass.add_quiter(k.quit)

mapperclass.run()
