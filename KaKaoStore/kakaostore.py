import sys
import os
import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from time import sleep

sys.path.append('/'.join(os.path.abspath(__file__).split('/')[:-2]))

import seleniumcrawler
from utilty import Util


class KaKaoStore(seleniumcrawler.SeleniumCrawler):
    def login(self, cookie_url):
        cookies = self.cookie_check()
        if cookies is None:
            self.driver.find_element_by_css_selector('#mArticle > div > div > button').click()
            WebDriverWait(self.driver, 3).until(EC.url_changes('https://accounts.kakao.com/login?continue=https%3A%2F%2Fcomm-auth-web.kakao.com%2Flogin%2Fcheck?hash=MVRHCQ7J8lYVNPNIasyqr8FREiIxGeEnqioHYiShMT8'))

            self.driver.find_element_by_css_selector('#loginEmail').send_keys(self.id)
            self.driver.find_element_by_css_selector('#loginPw').send_keys(self.password)
            self.driver.find_element_by_css_selector('#login-form > fieldset > button').click()
            WebDriverWait(self.driver,3).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#kakaoWrap')))

            self.driver.implicitly_wait(1)

            self.driver.find_element_by_css_selector('#mArticle > div.inner_comm > div.shop_list > div:nth-child(2) > button').click()

            WebDriverWait(self.driver, 4).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#kakaoWrap')))
            sleep(1)
        else:
            for i in cookies:
                self.driver.add_cookie(i)
            self.driver.get(cookie_url)
            self.driver.implicitly_wait(2)
            WebDriverWait(self.driver, 4).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#kakaoWrap')))
            sleep(1)
            self.put_message('Cookie Login')

            #
        self.save_cookie()

        self.put_message('Login Successful')
        self.save_login_image()

    def get_username(self):
        return self.driver.find_element_by_css_selector('#mAside > wf-view-dashboard-profile > div > strong').get_attribute('innerHTML')

    @Util.Report
    def item(self):
        # 30 days
        # self.driver.find_element_by_link_text(u"상품조회/수정").click()
        self.driver.get('https://store-sell.kakao.com/product/list')
        WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#mArticle > form > fieldset > div > wf-view-product-status > div > div > a:nth-child(1)')))
        self.driver.implicitly_wait(1)
        self.driver.find_element_by_css_selector('#mArticle > form > fieldset > div > div.info_write.adjust_terms > div.wrap_item.item_term > wf-view-period-palette > span > button:nth-child(4)').click()
        self.driver.find_element_by_css_selector('#mArticle > form > fieldset > div > div.wrap_btn > button.btn_search').click()
        WebDriverWait(self.driver, 1).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#mArticle > wf-view-grid > div.wf-view-grid > div.loading_layer')))
        self.driver.implicitly_wait(2)
        sleep(2)

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_css_selector('#mArticle > form > fieldset > div > div.info_write.adjust_table > div.item_ctr > a').click()

        self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xlsx'), 'xlsx')

        return self.message_list

    @Util.Report
    def settlement(self):
        # Default 1month
        self.driver.find_element_by_css_selector('#kakaoGnb > ul > li:nth-child(4) > a').click()
        WebDriverWait(self.driver, 4).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#page-search-box_AX_2_AX_1_AX_button')))
        sleep(1)
        self.driver.implicitly_wait(1)
        self.driver.find_element_by_css_selector('#page-search-box_AX_2_AX_0_AX_button').click()
        sleep(1)
        WebDriverWait(self.driver, 4).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#page-grid-box_AX_tbody > tr')))
        sleep(2)

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_css_selector('#page-search-box_AX_2_AX_2_AX_button').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xlsx'), 'xlsx')

        return self.message_list

    @Util.Report
    def cscollect(self):
        # Default 90 days
        self.driver.get('https://store-sell.kakao.com/qna/list')
        WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#mArticle > form > fieldset > div > div:nth-child(6) > div.wrap_item > button')))
        self.driver.find_element_by_css_selector('#mArticle > form > fieldset > div > div.wrap_btn > button.btn_search').click()

        sleep(2)
        self.driver.implicitly_wait(2)

        self.enable_download()
        file_list = self._get_files_in_save_dir()
        self.driver.find_element_by_css_selector('#mArticle > form > fieldset > div > div.wrap_btndown > a').click()
        self._change_name_of_file(self._wait_until_download_is_complete(file_list, 'xlsx'), 'xlsx')

        return self.message_list

    @Util.Report
    def paid(self):
        self.driver.get('https://store-buy-sell.kakao.com/order/list')
        self.driver.implicitly_wait(1)
        window = self.driver.window_handles
        self.driver.switch_to_window(window[1])
        WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#pageRequest > form > fieldset > div.wrap_btn > a')))
        self.driver.implicitly_wait(1)


        return
        #
        #HERE API
        #
        self.driver.find_element_by_css_selector('#requestPasscode').send_keys("INPUT")
        self.driver.find_element_by_css_selector('#pageRequest > form > fieldset > div.wrap_btn > a').click()

        return self.message_list

    @Util.Report
    def cancel(self):
        self.driver.get('https://store-buy-sell.kakao.com/order/cancelList')
        self.driver.implicitly_wait(1)
        window = self.driver.window_handles
        self.driver.switch_to_window(window[1])
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, '#pageRequest > form > fieldset > div.wrap_btn > a')))
        self.driver.implicitly_wait(1)

        return
        #
        # HERE API
        #
        self.driver.find_element_by_css_selector('#requestPasscode').send_keys("INPUT")
        self.driver.find_element_by_css_selector('#pageRequest > form > fieldset > div.wrap_btn > a').click()

        return self.message_list

    @Util.Report
    def orderreturn(self):
        self.driver.get('https://store-buy-sell.kakao.com/order/returnList')
        self.driver.implicitly_wait(1)
        window = self.driver.window_handles
        self.driver.switch_to_window(window[1])
        WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, '#pageRequest > form > fieldset > div.wrap_btn > a')))
        self.driver.implicitly_wait(1)

        return
        #
        # HERE API
        #
        self.driver.find_element_by_css_selector('#requestPasscode').send_keys("INPUT")
        self.driver.find_element_by_css_selector('#pageRequest > form > fieldset > div.wrap_btn > a').click()

        return self.message_list

# k = KaKaoStore(1008, 1006, 'thessadagu@gmail.com', 'dkswlals113@', 'https://sell.kakao.com/')
# k.login('https://store-sell.kakao.com/dashboard')
# print(k.get_username())